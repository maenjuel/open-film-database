<!doctype html>
<html class="no-js" lang="en-GB">

<head>
	<title>JavaScript licenses [Open Film Database]</title>
	<meta charset="UTF-8">
	<meta name="description" content="The Open Film Database a free and open database for movies and TV shows. It is a project maintained by its community.">
	<meta name="keywords" content="Open Film Database, OFDb, Film, Movie, TV Show, TV Series">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="theme-color" content="#2e3440">

	<meta property="og:site_name" content="Open Film Database" />
	<meta property="og:title" content="Open Film Database" />
	<meta property="og:description" content="The Open Film Database a free and open database for movies and TV shows. It is a project maintained by its community." />
	<meta property="og:type" content="website" />
	<meta property="og:locale" content="en_GB" />
	<meta property="og:url" content="https://ofdb.cc" />

	<link rel="stylesheet" href="/vendor/google/@material-icons/css/all.css" />
	<link rel="stylesheet" href="/css/ofdb.min.css" />

	<!-- Matomo -->
	<script type="text/javascript">
		// @license magnet:?xt=urn:btih:1f739d935676111cfff4b4693e3816e664797050&dn=gpl-3.0.txt GPL-v3-or-Later
		var _paq = window._paq = window._paq || [];
		/* tracker methods like "setCustomDimension" should be called before "trackPageView" */
		_paq.push(['trackPageView']);
		_paq.push(['enableLinkTracking']);
		(function() {
			var u="//matomo.ofdb.cc/";
			_paq.push(['setTrackerUrl', u+'matomo.php']);
			_paq.push(['setSiteId', '1']);
			var d=document, g=d.createElement('script'), s=d.getElementsByTagName('script')[0];
			g.type='text/javascript'; g.async=true; g.src=u+'matomo.js'; s.parentNode.insertBefore(g,s);
		})();
		// @license-end
	</script>
	<!-- End Matomo Code -->
</head>

<body class="javascript-licenses">
	<!--[if IE]>
		<p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="https://browsehappy.com/">upgrade your browser</a> to improve your experience and security.</p>
	<![endif]-->

	<div id="wrapper">
		<h1 class="title">JavaScript licenses</h1>
		<p>This website only uses <a href="https://www.gnu.org/philosophy/free-sw.html" target="_blank" title="What is Free Software?">free/libre</a> JavaScript and is compatible with <a href="https://www.gnu.org/software/librejs/" target="_blank" title="GNU LibreJS">GNU LibreJS</a>.</p>
		<table id="jslicense-labels1">
			<thead>
				<tr>
					<td>Script</td>
					<td>License</td>
					<td>Source</td>
				</tr>
			</thead>
			<tbody>
				<tr>
					<td><a href="/vendor/ekalinin/typogr.js/typogr.min.js" target="_blank">typogr.min.js</a></td>
					<td><a href="http://www.jclark.com/xml/copying.txt" target="_blank">Expat</a></td>
					<td><a href="https://github.com/ekalinin/typogr.js/blob/0.6.8/LICENSE" target="_blank">typogr.js 0.6.8</a></td>
				</tr>
				<tr>
					<td><a href="/js/ofdb.min.js" target="_blank">ofdb.min.js</a></td>
					<td><a href="http://www.gnu.org/licenses/gpl-3.0.html" target="_blank">GPLv3 or later</a></td>
					<td><a href="https://gitlab.com/maenjuel/open-film-database/-/blob/master/LICENSE" target="_blank">Open Film Database</a></td>
				</tr>
			</tbody>
		</table>
		<p><a href="/" alt="Back" title="Back"><i class="icon material-icons md-24 md-keyboard_arrow_left"></i> back</a></p>
	</div> <!-- /#wrapper -->
</body>

</html>
