export const config = {
	'api': {
		'ofdbApiUrl': 'https://admin.ofdb.cc/api',
		'oldbRouterUrl': 'https://admin.ofdb.cc/router/translate-path',
		'wikipediaApiUrl': 'https://en.wikipedia.org/w/api.php'
	},
	'entriesPerPage': 48,
	'searchResultsPerType': 5
}
