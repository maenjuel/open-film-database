import { PAGE_TITLE, PAGE_CONTENT, PATH } from './constants.js';

// Add classes to body tag
function addBodyClasses(classes) {
	if(!Array.isArray(classes)) {
		console.log('Classes need to be provided as an Array, will ignore them.');
		classes = [];
	}

	document.body.className += classes;
}

// Display messages
function displayMessage(options) {
	var type = options.type;
	var message = options.message;
	var container = document.getElementById('messages');
	var icon;

	if (options.type == 'error') {
		icon = '<i class="icon left material-icons-outline md-18 md-error"></i>';
	} else if (options.type == 'success') {
		icon = '<i class="icon left materiherzlicheal-icons-outline md-18 md-thumb_up"></i>';
	} else {
		icon = '<i class="icon left material-icons-outline md-18 md-info"></i>'; // Fallback icon
		type = 'info';
	}

	if(options.message != undefined) {
		message = options.message;
	} else {
		message = 'We encountered an error, please try again.';
	}

	container.classList.add('show');
	container.classList.add(type); // Add class with the name of the message type
	container.innerHTML = icon + '<span>' + typogr.typogrify(message) + '</span>';
}

// Get header height
function headerHeight() {
	let headerElement = document.getElementById('site-header');
	return headerElement.offsetHeight;
}

// Define breakpoints
let windowWidth = window.innerWidth;

function isMobile() {
	if(windowWidth < 960) {
		return true;
	} else {
		return false;
	}
}

function displayLoader(id) {
	let el = document.getElementById(id);
	let loader = document.getElementById('loader');

	if(!loader) { // We don't want multiple loaders on the screen
		if(el) {
			el.innerHTML += '<div id="loader"><svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" style="margin: 1em auto; background: rgba(0, 0, 0, 0) none repeat scroll 0% 0%; display: block; shape-rendering: auto;" width="30px" height="30px" viewBox="0 0 100 100" preserveAspectRatio="xMidYMid"><circle cx="50" cy="50" r="45" stroke-width="8" stroke="#eceff4" stroke-dasharray="70.68583470577035 70.68583470577035" fill="none" stroke-linecap="round"><animateTransform attributeName="transform" type="rotate" repeatCount="indefinite" dur="1.25s" keyTimes="0;1" values="0 50 50;360 50 50"></animateTransform></circle></div>';
		} else {
				let body = document.body;
				let loaderWrapper = document.getElementById('loader-wrapper');

				loaderWrapper.innerHTML += '<div id="loader"><svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" style="margin: ' + headerHeight() / 2 + 'px auto 0 auto; background: rgba(0, 0, 0, 0) none repeat scroll 0% 0%; display: block; shape-rendering: auto;" width="50px" height="50px" viewBox="0 0 100 100" preserveAspectRatio="xMidYMid"><circle cx="50" cy="50" r="45" stroke-width="8" stroke="#eceff4" stroke-dasharray="70.68583470577035 70.68583470577035" fill="none" stroke-linecap="round"><animateTransform attributeName="transform" type="rotate" repeatCount="indefinite" dur="1.25s" keyTimes="0;1" values="0 50 50;360 50 50"></animateTransform></circle></svg></div>';
				loaderWrapper.setAttribute('style', 'display: block');

				// let's make the page non-scrollable while the loader is displayed
				body.style.overflow = 'hidden';
		}
	}
}

function metaTags(metaTags) {
	let title = metaTags.title;
	let description = metaTags.description;
	let keywords = metaTags.keywords;
	let meta = document.getElementsByTagName('meta');

	if(metaTags.hasOwnProperty('title') || title != undefined) {
		document.title = title + ' [Open Film Database]';
	}

	if(metaTags.hasOwnProperty('description') || description != undefined) {
		document.querySelector('meta[name="description"]').setAttribute('content', description);
	}

	if(metaTags.hasOwnProperty('keywords') || keywords != undefined) {
		keywords = document.querySelector('meta[name="keywords"]').content + ', ' + keywords.join(', ');
		document.querySelector('meta[name="keywords"]').setAttribute('content', keywords);
	}
}

function openGraph(og) {
	let title = og.title;
	let description = og.description;
	let type = og.type;
	let url = og.url;

	if(og.hasOwnProperty('title') || title != undefined) {
		document.querySelector('meta[property="og:title"]').setAttribute('content', title  + ' [Open Film Database]');
	}

	if(og.hasOwnProperty('description') || description != undefined) {
		document.querySelector('meta[property="og:description"]').setAttribute('content', description);
	}

	if(og.hasOwnProperty('type') || type != undefined) {
		document.querySelector('meta[property="og:type"]').setAttribute('content', type);
	}

	if(og.hasOwnProperty('url') || url != undefined) {
		document.querySelector('meta[property="og:url"]').setAttribute('content', url);
	} else {
		document.querySelector('meta[property="og:url"]').setAttribute('content', 'https://' + location.hostname + PATH);
	}
}

function pageNotFound(type) {
	let title = 'Uh oh...';
	let message;

	if (type == 'movie') {
		message = 'We couldn\'t find the requested movie.';
	} else if (type == 'tv_show') {
		message = 'We couldn\'t find the requested TV show.';
	} else {
		message = 'We couldn\'t find the requested page.';
	}

	PAGE_TITLE.innerHTML = typogr.typogrify(title);
	PAGE_CONTENT.innerHTML = typogr.typogrify('<p>' + message + '</p><p><a href="/">Go back to the home page</a>.</p>');

	metaTags({
		'title': title
	});
}

function removeLoader() {
	let loaderWrapper = document.getElementById('loader-wrapper');
	let loader = document.getElementById('loader');
	if(loader != null) {
		loader.parentNode.removeChild(loader);

		// let's make the page scrollable again
		let body = document.body;
		body.style.overflow = null;
		loaderWrapper.style.removeProperty('display');
	}
}

export { addBodyClasses, displayMessage, headerHeight, isMobile, displayLoader, metaTags, openGraph, pageNotFound, removeLoader };
