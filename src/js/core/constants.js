export const CONTENT = '/assets/content.json';
export const PAGE_CONTENT = document.getElementById('content');
export const PAGE_TITLE = document.getElementById('title');
export const PARAMS = new URLSearchParams(window.location.search);
export const PATH = window.location.pathname;
export const SEARCHBOX = document.getElementById('searchbox').getElementsByTagName('input')[0];
export const SUGGESTIONS = document.getElementById('suggestions');
export const URL = window.location.href;
