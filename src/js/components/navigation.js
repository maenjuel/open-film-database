import { CONTENT, PATH } from '../core/constants.js';
import { isMobile } from '../core/global.js';

let mainMenuContainer = document.getElementById('main-menu');
let mainMenu = document.getElementById('main-menu').getElementsByTagName('ul')[0];
let footerMenu = document.getElementById('footer-menu').getElementsByTagName('ul')[0];
let footerMenuItems = []; // We'll populate that later

// Add mobile menu functionalities
function addHamburgerEventListener() {
	let menuToggler = document.getElementById('menu-toggler');
	menuToggler.addEventListener('click', toggleMenu);
}

function toggleMenu() {
	mainMenuContainer.classList.toggle('show');
}

// Populate page menus
function buildMenus() {
	// Populate menus with the links
	fetch(CONTENT)
		.then(res => res.json())
		.then(json => {
			json = json.page;

			json.forEach(page => {
				let path = page.path;
				let name = page.menu.name;
				let position = page.menu.position;
				let rel = page.menu.rel;
				let linkClass;

				if(path == PATH) {
					linkClass = 'class="active"';
				} else {
					linkClass = '';
				}

				if(position == 'header') {
					if(rel != '') {
						rel = 'rel="' + rel + '"';
					}

					mainMenu.innerHTML += '<li><a href="' + path + '"' + linkClass + ' alt="' + name + '"title="' + name + '"' + rel + '>' + name + '</a></li>';
				} else {
					if(rel != '') {
						rel = 'rel="' + rel + '"';
					}

					if(position != undefined) {
						// First we store the menu items in an array and add them later since the footer menu already contains items
						// Like this we have more control over how to add the items to the menu
						let menuItem = '<li><a href="' + path + '"' + linkClass + ' alt="' + name + '"title="' + name + '"' + rel + '>' + name + '</a></li>';
						footerMenuItems.push(menuItem);
					}
				}
			});

			// Add login link
			mainMenu.innerHTML += '<li><a href="https://admin.ofdb.cc/user/login" alt="Login" title="Login" target="_blank">Login</a></li>';

			// Add footer menu items
			footerMenuItems.reverse().forEach(item => {
				footerMenu.insertAdjacentHTML('afterbegin', item);
			});

			// Add event listener for hamburger icon
			addHamburgerEventListener();
		})
		.catch(err => {
			console.log('Error loading menus: ' + err);
		});

	// If we are on the desktop, we need to add the .show class
	if(!isMobile()) {
		mainMenuContainer.classList.add('show');
	}
}

export { buildMenus };
