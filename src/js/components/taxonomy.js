import { config } from '../core/config.js';

function fetchTags(vocabulary='genre', limit=50, offset=0, sort='name', filter) {
	let url = config.api.ofdbApiUrl + '/taxonomy_term/' + vocabulary + '/?page[limit]=' + limit + '&page[offset]=' + offset + '&filter[status][value]=1' + filter + '&sort=' + sort;

	return fetch(url)
		.then(res => res.json())
		.then(json => {
			return json;
		})
		.catch(err => console.log(err));
}

function fetchTag(vocabulary='genre', id) {
	let url;

	if(id) {
		url = config.api.ofdbApiUrl + '/taxonomy_term/' + vocabulary + '/' + id;
	} else {
		url = config.api.ofdbApiUrl + '/taxonomy_term/' + vocabulary;
	}

	return fetch(url)
		.then(res => res.json())
		.then(json => {
			return json;
		})
		.catch(err => console.log(err));
}

export { fetchTags, fetchTag };
