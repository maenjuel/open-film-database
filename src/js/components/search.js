import  { config } from '../core/config.js';
import  { SEARCHBOX, SUGGESTIONS } from '../core/constants.js';
import  { displayLoader } from '../core/global.js';
import  { fetchFilms } from './film.js';

function buildSearchData(search) {
	let filter = '&filter[title-group][group][conjunction]=OR&filter[title-filter][condition][path]=title&filter[title-filter][condition][operator]=CONTAINS&filter[title-filter][condition][value]=' + search + '&filter[title-filter][condition][memberOf]=title-group&filter[original-filter][condition][path]=field_original_title&filter[original-filter][condition][operator]=CONTAINS&filter[original-filter][condition][value]=' + search + '&filter[original-filter][condition][memberOf]=title-group'; // We create a filter group to search the title and original title
	let movies = fetchFilms('movie', config.searchResultsPerType, 0, filter, 'title');
	let shows = fetchFilms('tv_show', config.searchResultsPerType, 0, filter, 'title');

	Promise.all([movies, shows])
		.then(res => {
			let films = [];
			let total = res[0].data.length + res[1].data.length;

			if(total == 0) {
				SUGGESTIONS.innerHTML = typogr.typogrify('<div class="messages">No results found...</div>');
				return;
			}

			res.forEach(type => {
				type.data.forEach(film => {
					let id = film.id;
					let title = film.attributes.title;
					let type = film.type.split('--').pop();

					film = {
						'id': id,
						'title': title,
						'type': type
					}

					films.push(film);

					if(films.length == total) {
						displaySearchData(films);
					}
				});
			});
		})
		.catch(err => console.log(err));
}

function displaySearchData(films) {
	SUGGESTIONS.innerHTML = '<ul></ul>';
	let list = SUGGESTIONS.getElementsByTagName('ul')[0];

	films.sort((a, b) => (a.title > b.title) ? 1 : ((b.title > a.title) ? -1 : 0));
	films.forEach(film => {
		let id = film.id;
		let title = film.title;
		let type = film.type;
		let urlprefix;

		if(type == 'movie') {
			type = '<i class="icon material-icons md-18 md-movie"></i>';
			urlprefix = 'movie/';
		} else {
			type = '<i class="icon material-icons md-18 md-tv"></i>';
			urlprefix = 'tv-show/';
		}

		list.innerHTML += '<li><a href="/' + urlprefix + id + '" alt="' + title + '" title="' + title + '">' + type + '<span>' + typogr.typogrify(title) + '</span></a></li>'
	});
}

function search() {
	if(SEARCHBOX.value.length == 0) {
		SUGGESTIONS.classList.remove('show');
	} else if(SEARCHBOX.value.length > 0 && SEARCHBOX.value.length < 3) {
		SUGGESTIONS.innerHTML = typogr.typogrify('<div class="messages">Please enter three or more characters to search.</div>');
		SUGGESTIONS.classList.add('show');
	} else {
		SUGGESTIONS.innerHTML = '';
		displayLoader('suggestions');
		buildSearchData(SEARCHBOX.value);
		SUGGESTIONS.classList.add('show');
	}
}

function searchListener() {
	// Trigger search
	SEARCHBOX.addEventListener('keyup', search);

	// Toggle suggestions visibility on click in- our outside suggestions
	document.body.addEventListener('click', function (event) {
		let searchContainer = document.getElementById('search');

		if(!searchContainer.contains(event.target) && SUGGESTIONS.classList.contains('show')) {
			SUGGESTIONS.classList.remove('show');
		} else {
			if(SEARCHBOX.value != '') {
				SUGGESTIONS.classList.add('show');
			}
		}
	});
}

export { searchListener };
