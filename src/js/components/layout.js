import { headerHeight } from '../core/global.js';

function contentOffset() {
	let contentElement = document.getElementById('site-content');
	contentElement.style.marginTop = headerHeight() + 'px';
}

export { contentOffset };
