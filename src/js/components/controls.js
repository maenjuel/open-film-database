import { PARAMS, PATH, URL } from '../core/constants.js';
import { headerHeight } from '../core/global.js';
import { fetchTags } from './taxonomy.js';
import { fetchFilms } from './film.js';

function buildURLParams(type, parameters, url) {

	// Check parameters
	if(!type || !parameters) {
		console.log('No parameters received. Nothing to do.');
		return;
	} else if(!Array.isArray(parameters)) {
		console.log('Parameter must be an array.');
		return;
	} else {
		parameters = parameters.join(',');
	}

	// If no URL parameter is provided, we simply take the currenty URL
	if(!url) {
		url = URL;
	}

	let params = url.split('?')[1];

	// We'll need to see whether the provided type is already in the URL, hence we need all types in
	// An array (since a parameter might be the same as a type we need to know specifically the types)
	let types = [];
	let nrOfTypes = 0;
	let typeIncluded = false;

	if(params) {
		types.push(params.split('=')[0]); // The first parameter directly after "?"
		for(let i = 0; i < params.length; i++) {
			if(params.charAt(i) == '&'){
				nrOfTypes ++;
			}
		}

		for(let i = 0; i < nrOfTypes; i++) {
			let type = params.split('&')[1];
			type = type.split('=')[0];
			types.push(type);
		}

		types.forEach(t => {
			if(t == type) {
				typeIncluded = true;
			}
		});
	}

	// Now we build all parameters
	if(!params) {
		params = '?' + type + '=' + parameters;
	} else if(typeIncluded) {
		let splitParams = params.split(type + '=');
		let typeParams;

		if(splitParams[1] && splitParams[1].includes('&')) {
			let typeLength = splitParams[1].indexOf('&');
			typeParams = splitParams[1].substring(typeLength);
		} else {
			typeParams = '';
		}

		// remove possible duplicates (if value is already in url, and provided as parameter)
		parameters = [...new Set(parameters.split(','))].join(',');

		// Finally we put everything together
		params = '?' + splitParams[0] + type + '=' + parameters + typeParams;
	} else {
		if(params.includes('sort')) {
			// We want sort at the end of the path
			let splitParams = params.split('sort=');

			params = '?' + splitParams[0] + '&' + type + '=' + parameters + '&sort=' + splitParams[1];

			// Because we moved the sort= parameter, but the "&" or is still there, we remove the leftover character
			params = params.replace('?&', '?');
			params = params.replace('&&', '&');
		} else {
			params = '?' + params + '&' + type + '=' + parameters;
		}
	}

	return params;
}

function removeURLParams(type, url) {
	// If no URL parameter is provided, we simply take the currenty URL
	if(!url) {
		url = URL;
	}

	let params = url.split('?')[1];
	let splitParams = params.split(type + '=');
	let typeParams;

	if(splitParams[1] && splitParams[1].includes('&')) {
		let typeLength = splitParams[1].indexOf('&');
		typeParams = splitParams[1].substring(typeLength);
	} else {
		typeParams = '';
	}

	params = '?' + splitParams[0] + typeParams;

	// Remove leftover characters after removal
	params = params.replace('?&', '?');
	params = params.replace('&&', '&');
			
	if(params.endsWith('?') || params.endsWith('&')) {
		params = params.substring(0, params.length - 1);
	}
					
	return params; // Return parameters
}

function toggleFilters() {
	// Toggle class
	let filtersContainer = document.getElementById('filters');
	filtersContainer.classList.toggle('show');

	// Change button text
	let button = document.getElementById('toggle-filters');
	if(button.innerText.includes('Show')) {
		button.innerHTML = 'Hide filters <i class="icon material-icons md-18 md-keyboard_arrow_up"></i>';
	} else {
		button.innerHTML = 'Show filters <i class="icon material-icons md-18 md-keyboard_arrow_down"></i>';
	}
}

function dateFilter() {
	let dateContainer = document.getElementById('filter-date');
	let dateInput = dateContainer.getElementsByTagName('input');
	let messageContainer = document.getElementById('date-message');
	messageContainer.innerHTML = ''; // Start with an empty field

	let from = dateInput[0].value;
	let to = dateInput[1].value;

	if(from && to) {
		from = new Date(dateInput[0].value);
		to = new Date(dateInput[1].value);

		if(from > to) {
			messageContainer.innerHTML = typogr.typogrify('"From" field must be earlier than "to" field.');
		} else {
			function twoDigitDate(date) {
				if(date < 10) {
					date = '0' + date;
				}

				return date;
			}

			from = from.getFullYear() + '-' + twoDigitDate((from.getMonth() + 1)) + '-' + twoDigitDate(from.getDate());
			to = to.getFullYear() + '-' + twoDigitDate((to.getMonth() + 1)) + '-' + twoDigitDate(to.getDate());

			window.location.replace(PATH + buildURLParams('release_date', [from, to]));
			//console.log(PATH, buildURLParams('release_date', [from, to]), PATH + buildURLParams('release_date', [from, to]));
			
		}
	}
}

function displayControls() {
	// Build URL params
	let url = URL;
	let sort = PARAMS.get('sort');

	if(!sort) {
		sort = 'title';
	}

	let urlParams;
	if(!url.includes('?')) {
		url = url + '?';
	} else if(url.includes('sort=')) {
		url = url.split('sort=');

		if(url[1].includes('&')) {
			let sortLength = url[1].indexOf('&');
			urlParams = url[1].substring(sortLength);
		} else {
			urlParams = '';
		}

		url = url[0] + urlParams;
	} else {
		// If there is no "sort" parameter, we need to add a "&" to the URL before appending the sort parameter
		url = url + '&';
	}

	// Get sort order for buttons
	let preTitle;
	let preRelease;
	let arrow; // We'll use that after displaying the links iteselves

	if(sort.includes('-')) {
		preTitle = '';
		preRelease = '';
		arrow = '<i class="icon material-icons md-18 md-keyboard_arrow_down"></i>';
	} else if(sort == 'field_release_date') {
		preTitle = '';
		preRelease = '-';
		arrow = '<i class="icon material-icons md-18 md-keyboard_arrow_up"></i>';
	} else {
		preTitle = '-';
		preRelease = '';
		arrow = '<i class="icon material-icons md-18 md-keyboard_arrow_up"></i>';
	}

	// Display controls
	let controlsContainer = document.getElementById('controls');

	if(PATH.includes('/movies')) {
		controlsContainer.innerHTML += '<div class="row"><button class="secondary" id="toggle-filters">Show filters <i class="icon material-icons md-18 md-keyboard_arrow_down"></i></button><div id="sorting">Sort: <a href="' + url + 'sort=' + preTitle + 'title" id="sort-title">Title</a> | <a href="' + url + 'sort=' + preRelease + 'field_release_date" id="sort-field_release_date">Release date</a></div><div id="filters"><div id="filter-genre"><strong>Genre</strong></div><div id="grouped"><div id="filter-date"><strong>Release date</strong></div><div id="filter-short"><strong>Short film</strong></div><div id="filter-independent"><strong>Independent</strong></div></div>';
	} else {
		controlsContainer.innerHTML += '<div class="row"><button class="secondary" id="toggle-filters">Show filters <i class="icon material-icons md-18 md-keyboard_arrow_down"></i></button><div id="sorting">Sort: <a href="' + url + 'sort=' + preTitle + 'title" id="sort-title">Title</a></div><div id="filters"><div id="filter-genre"><strong>Genre</strong></div><div id="filter-status"><strong>Status</strong></div></div></div>';
	}

	// Add class and appropriate arrow icon to active sort
	let activeId = 'sort-' + sort.split('-').pop();
	let activeLink = document.getElementById(activeId);
	activeLink.innerHTML += arrow;
	activeLink.classList.add('active');

	// Add content offset (due to position: fixed)
	let contentElement = document.getElementById('site-content');
	let contentRow = contentElement.lastElementChild;
	let controlsHeight = controlsContainer.offsetHeight;
	contentRow.style.marginTop = controlsHeight + 'px';

	// Add genre filter
	let genreContainer = document.getElementById('filter-genre');
	genreContainer.innerHTML += '<ul></ul>';
	let genreList = genreContainer.getElementsByTagName('ul')[0];

	fetchTags()
		.then(res => {
			let data = res.data;
			data.forEach(tag => {
				// Build URL
				let genres = PARAMS.get('genres');
				let genreParams = [];

				if(genres != null) {
					genres = genres.replace('?genres=', ''); // We need to remove ?genres= since it's also part of the string
					genreParams = genres.split(',');
				} else {
					genres = ''; // We simply add an empty value so the app doesn't complain about genres = null
				}

				genreParams.push(tag.id);
				let url = PATH + buildURLParams('genres', genreParams);

				// If the genre is already in the URL, we need to remove it again, since we want to toggle them
				if(genres.includes(tag.id) && genres.startsWith(tag.id) && genres.endsWith(tag.id)) {
					url = PATH + removeURLParams('genres');
				} else if(genres.includes(tag.id) && genres.endsWith(tag.id)) {
					url = url.replace(',' + tag.id, '');
				} else if(genres.includes(tag.id)) {
					url = url.replace(tag.id + ',', '');
				}

				// If we remove all genres, but some parameters follow, the URL could possibly look like this: PATH&type=parameters
				// If that is the case, we need to replace the first "&" with a "?"
				if(url.includes('&') && !url.includes('?')) {
					url = url.replace('&', '?');
				}

				// Get correct icon (checked or unchecked)
				let icon;
				if(genres.includes(tag.id)) {
					icon = '<i class="icon material-icons-outline md-18 md-check_box"></i>';
				} else {
					icon = '<i class="icon material-icons-outline md-18 md-check_box_outline_blank"></i>';
				}

				// Add item to list
				if(PATH.includes('/movies')) {
					// For movies we don't want the 'sitcom' and 'reality' genre in the list, as this applies for TV shows only
					if(tag.id != '0d65942f-0a7e-457c-8799-82f07113ec06' && tag.id != '0fe0c0c3-aae1-4d26-9a3f-2affb9044a06') {
						genreList.innerHTML += '<li id="' + tag.id + '"><a href="' + url + '">' + icon + tag.attributes.name + '</a></li>';
					}
				} else {
					genreList.innerHTML += '<li id="' + tag.id + '"><a href="' + url + '">' + icon + tag.attributes.name + '</a></li>';
				}
			});
		})
		.catch(err => console.log(err));

	// Add event listener for the genre list
	genreList.addEventListener('click', function(e) {
		if (e.target && e.target.matches('li')) {
			toggleGenreFilter(e.target.id);
		}
	});

	// Add Release date filter
	let type;
	if(PATH.includes('/movies')) {
		type = 'movie';
	} else {
		type = 'tv_show';
	}

	// Display movie- or TV show specific filters
	if(type == 'movie') {
		// Release date filter
		fetchFilms(type, 1, 0, '&filter[field_release_date][operator]=CONTAINS&filter[field_release_date][value]=-', 'field_release_date')
			.then(json => {
				let releaseParams = PARAMS.get('release_date');
				let releaseDate = json.data[0].attributes.field_release_date;
				let now = new Date();
				let maxDate = now.getFullYear() + '-' + (now.getMonth() + 1) + '-' + now.getDate();
				let dateContainer = document.getElementById('filter-date');

				if(releaseParams) {
					releaseParams = releaseParams.split(',');

					// Build URL for reset button
					let url = URL.split('release_date=');
					let releaseLength = url[1].indexOf('&');
					let splitURL;

					if(releaseLength != -1) {
						url = url[0] + url[1].substring(releaseLength);

						if(url.includes('?&')) {
							url = url.replace('?&', '?');
						} else if(url.includes('&&')) {
							url = url.replace('&&', '&');
						}
					} else {
						url = url[0];
						if(url.endsWith('?') || url.endsWith('&')) {
							url = url.substring(0, url.length - 1);
						}
					}

					// Display forms and reset button
					dateContainer.innerHTML += '<div><input type="date" min="' + releaseDate + '" max="' + maxDate + '" value="' + releaseParams[0] + '" /> &ndash; <input type="date" min="' + releaseDate + '" max="' + maxDate + '" value="' + releaseParams[1] + '" /><span id="date-message"></span> <a href="' + url + '" class="button secondary">reset</a></div>';
				} else {
					dateContainer.innerHTML += '<div><input type="date" min="' + releaseDate + '" max="' + maxDate + '" placeholder="' + releaseDate + '" /> &ndash; <input type="date" min="' + releaseDate + '" max="' + maxDate + '" placeholder="' + maxDate + '" /><span id="date-message"></span></div>';
				}

				// Add event listener for the release date filter
				let dateInput = dateContainer.getElementsByTagName('input');
				for (let i = 0; i < dateInput.length; i++) {
					dateInput[i].addEventListener('change', dateFilter);
				}
			})
			.catch(err => console.log(err));

			// Short film filter
			let short = PARAMS.get('short');
			let shortContainer = document.getElementById('filter-short');

			if(short == '1') {
				shortContainer.innerHTML += '<br /><a class="active"><i class="icon material-icons-outline md-18 md-radio_button_checked"></i> yes</a><a href="' + PATH + buildURLParams('short', ['0']) + '"><i class="icon material-icons-outline md-18 md-radio_button_unchecked"></i> no</a><a href="' + PATH + removeURLParams('short') + '"><i class="icon material-icons-outline md-18 md-radio_button_unchecked"></i> both</a>';
			} else if(short == '0'){
				shortContainer.innerHTML += '<br /><a href="' + PATH + buildURLParams('short', ['1']) + '"><i class="icon material-icons-outline md-18 md-radio_button_unchecked"></i> yes</a><a class="active"><i class="icon material-icons-outline md-18 md-radio_button_checked"></i> no</a><a href="' + PATH + removeURLParams('short') + '"><i class="icon material-icons-outline md-18 md-radio_button_unchecked"></i> both</a>';
			} else {
				shortContainer.innerHTML += '<br /><a href="' + PATH + buildURLParams('short', ['1']) + '"><i class="icon material-icons-outline md-18 md-radio_button_unchecked"></i> yes</a><a href="' + PATH + buildURLParams('short', ['0']) + '"><i class="icon material-icons-outline md-18 md-radio_button_unchecked"></i> no</a><a class="active"><i class="icon material-icons-outline md-18 md-radio_button_checked"></i> both</a>';
			}

			// Independent filter
			let indie = PARAMS.get('indie');
			let indieContainer = document.getElementById('filter-independent');

			if(indie == '1') {
				indieContainer.innerHTML += '<br /><a class="active"><i class="icon material-icons-outline md-18 md-radio_button_checked"></i> yes</a><a href="' + PATH + buildURLParams('indie', ['0']) + '"><i class="icon material-icons-outline md-18 md-radio_button_unchecked"></i> no</a><a href="' + PATH + removeURLParams('indie') + '"><i class="icon material-icons-outline md-18 md-radio_button_unchecked"></i> both</a>';
			} else if(indie == '0'){
				indieContainer.innerHTML += '<br /><a href="' + PATH + buildURLParams('indie', ['1']) + '"><i class="icon material-icons-outline md-18 md-radio_button_unchecked"></i> yes</a><a class="active"><i class="icon material-icons-outline md-18 md-radio_button_checked"></i> no</a><a href="' + PATH + removeURLParams('indie') + '"><i class="icon material-icons-outline md-18 md-radio_button_unchecked"></i> both</a>';
			} else {
				indieContainer.innerHTML += '<br /><a href="' + PATH + buildURLParams('indie', ['1']) + '"><i class="icon material-icons-outline md-18 md-radio_button_unchecked"></i> yes</a><a href="' + PATH + buildURLParams('indie', ['0']) + '"><i class="icon material-icons-outline md-18 md-radio_button_unchecked"></i> no</a><a class="active"><i class="icon material-icons-outline md-18 md-radio_button_checked"></i> both</a>';
			}
	} else {
		let status = PARAMS.get('status');
		let statusContainer = document.getElementById('filter-status');

		if(status == 'continuing') {
			statusContainer.innerHTML += '<br /><a class="active"><i class="icon material-icons-outline md-18 md-radio_button_checked"></i> continuing</a><a href="' + PATH + buildURLParams('status', ['ended']) + '"><i class="icon material-icons-outline md-18 md-radio_button_unchecked"></i> ended</a><a href="' + PATH + buildURLParams('status', ['cancelled']) + '"><i class="icon material-icons-outline md-18 md-radio_button_unchecked"></i> cancelled</a><a href="' + PATH + removeURLParams('status') + '"><i class="icon material-icons-outline md-18 md-radio_button_unchecked"></i> all</a>';
		} else if(status == 'ended') {
			statusContainer.innerHTML += '<br /><a href="' + PATH + buildURLParams('status', ['continuing']) + '"><i class="icon material-icons-outline md-18 md-radio_button_unchecked"></i> continuing</a><a class="active"><i class="icon material-icons-outline md-18 md-radio_button_checked"></i> ended</a><a href="' + PATH + buildURLParams('status', ['cancelled']) + '"><i class="icon material-icons-outline md-18 md-radio_button_unchecked"></i> cancelled</a><a href="' + PATH + removeURLParams('status') + '"><i class="icon material-icons-outline md-18 md-radio_button_unchecked"></i> all</a>';
		} else if(status == 'cancelled') {
			statusContainer.innerHTML += '<br /><a href="' + PATH + buildURLParams('status', ['continuing']) + '"><i class="icon material-icons-outline md-18 md-radio_button_unchecked"></i> continuing</a><a href="' + PATH + buildURLParams('status', ['ended']) + '"><i class="icon material-icons-outline md-18 md-radio_button_unchecked"></i> ended</a><a class="active"><i class="icon material-icons-outline md-18 md-radio_button_checked"></i> cancelled</a><a href="' + PATH + removeURLParams('status') + '"><i class="icon material-icons-outline md-18 md-radio_button_unchecked"></i> all</a>';
		} else {
			statusContainer.innerHTML += '<br /><a href="' + PATH + buildURLParams('status', ['continuing']) + '"><i class="icon material-icons-outline md-18 md-radio_button_unchecked"></i> continuing</a><a href="' + PATH + buildURLParams('status', ['ended']) + '"><i class="icon material-icons-outline md-18 md-radio_button_unchecked"></i> ended</a><a href="' + PATH + buildURLParams('status', ['cancelled']) + '"><i class="icon material-icons-outline md-18 md-radio_button_unchecked"></i> cancelled</a><a class="active"><i class="icon material-icons-outline md-18 md-radio_button_checked"></i> all</a>';
		}
	}

	// Add event listener for the toggle filter button
	let button = document.getElementById('toggle-filters');
	button.addEventListener('click', toggleFilters);
}

export { displayControls };
