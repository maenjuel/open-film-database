import { config } from '../core/config.js';

function fetchFilms(type='movie', limit=50, offset=0, filter='', sort='title') {
	let url = config.api.ofdbApiUrl + '/node/' + type + '/?page[limit]=' + limit + '&page[offset]=' + offset + '&filter[status][value]=1' + filter + '&sort=' + sort;

	return fetch(url)
		.then(res => res.json())
		.then(json => {
			return json;
		})
		.catch(err => console.log(err));
}

function fetchFilm(type='movie', id, include) {
	let inc;

	if(include) {
		inc = '&include=' + include.join(',');
	} else {
		inc = '';
	}

	let url = config.api.ofdbApiUrl + '/node/' + type + '/' + id + '/?filter[status][value]=1' + inc;
	return fetch(url)
		.then(res => res.json())
		.then(json => {
			return json;
		})
		.catch(err => console.log(err));
}

export { fetchFilms, fetchFilm };
