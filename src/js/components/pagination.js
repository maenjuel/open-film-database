import { config } from '../core/config.js';
import { PARAMS } from '../core/constants.js';
import { displayFilms } from '../content/content.js';

function mediaPagination(type, buttons = 'next', offset = 0) {
	let genres = PARAMS.get('genres');
	if(genres) {
		genres = genres.split(',');
		genres = '&filter[genre][condition][path]=field_genre.id&filter[genre][condition][operator]=IN&filter[genre][condition][value][]=' + genres.join('&filter[genre][condition][value][]=');
	}

	let release = PARAMS.get('release_date');
	if(release != null) {
		let from = release.split(',')[0];
		let to = release.split(',')[1];
		
		release = '&filter[from][condition][value]=' + from + '&filter[from][condition][path]=field_release_date&filter[from][condition][operator]=>&filter[to][condition][value]=' + to + '&filter[to][condition][path]=field_release_date&filter[to][condition][operator]=<'
	} else {
		release = ''; // Value mustn't be null, otherwise Drupal's JSON:API won't return anything
	}

	let status = PARAMS.get('status');
	if(status != null) {
		status = '&filter[field_status]=' + status;
	} else {
		status = '';
	}

	let sort = PARAMS.get('sort');
	if(!sort) {
		sort = 'title';
	}


	let container = document.getElementById('pagination');
	container.innerHTML = '<ul></ul>';
	let list = container.getElementsByTagName('ul')[0];

	if(buttons == 'next') {
		list.innerHTML = '<li id="next"><a class="button secondary" onclick="displayFilms(\'' + type + '\', ' + config.entriesPerPage + ', ' + (offset + config.entriesPerPage) + ', \'' + genres + release + status + '\', \'' + sort + '\')">next page &rsaquo;</a></li>';
	} else if(buttons == 'prev') {
		list.innerHTML = '<li id="previous"><a class="button secondary" onclick="displayFilms(\'' + type+ '\', ' + config.entriesPerPage + ', ' + (offset - config.entriesPerPage) + ', \'' + genres + release + status + '\', \'' + sort +  '\')">&lsaquo; previous page</a></li>';
	} else {
		list.innerHTML = '<li id="previous"><a class="button secondary" onclick="displayFilms(\'' + type+ '\', ' + config.entriesPerPage + ', ' + (offset - config.entriesPerPage) + ', \'' + genres + release + status + '\', \'' + sort + '\')">&lsaquo; previous page</a></li><li id="next"><a class="button secondary" onclick="displayFilms(\'' + type+ '\', ' + config.entriesPerPage + ', ' + (offset + config.entriesPerPage) + ', \'' + genres + '\', \'' + sort + '\')">next page &rsaquo;</a></li>';
	}
}

export { mediaPagination };
