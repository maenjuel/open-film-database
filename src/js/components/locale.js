function getCountryName(code) {
	let countryData = '../vendor/benkeen/country-region-data/data.json';

	return fetch(countryData)
		.then(res => res.json())
		.then(json => {
			let country = json.filter(c => (c.countryShortCode === code));
			return country[0].countryName;
		})
		.catch(err => { console.log(err) });
}

function getLanguageName(code) {
	let languageData = '../vendor/Datahub/language-codes/data/language-codes-full_json.json';
	code = code.split('-', 1)[0]; // Sometimes the language code looks like 'zh-', so we remove the dash

	return fetch(languageData)
		.then(res => res.json())
		.then(json => {
			let language;

			if(code.length == 3) {
				language = json.filter(lang => (lang['alpha3-b'] === code));
			} else {
				language = json.filter(lang => (lang.alpha2 === code));
			}

			return language[0].English;
		})
		.catch(err => { console.log(err) });
}

export { getCountryName, getLanguageName };
