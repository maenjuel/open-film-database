import { buildMenus } from './components/navigation.js';
import { contentOffset } from './components/layout.js';
import { displayContent } from './content/content.js';
import { searchListener } from './components/search.js';

// init app
(function init() {
	buildMenus();
	contentOffset();
	displayContent();
	searchListener();
})();
