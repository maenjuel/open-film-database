import { config } from '../core/config.js';
import { CONTENT, PAGE_TITLE, PAGE_CONTENT, PARAMS, PATH } from '../core/constants.js';
import { addBodyClasses, displayMessage, metaTags, openGraph, pageNotFound } from '../core/global.js';
import { displayControls } from '../components/controls.js';
import { displayFilms } from './content.js';

function displayPage() {
	fetch(CONTENT)
		.then(res => res.json())
		.then(json => {
			json = json.page;

			json.forEach(page => {
				// Add the appropriate page content
				if(page.path == PATH) {
					PAGE_TITLE.innerHTML = typogr.typogrify(page.title);
					PAGE_CONTENT.innerHTML = typogr.typogrify(page.content);

					// Set the correct meta tags
					metaTags({
						'title': page.title
					});

					// Set the correct open graphs
					openGraph({
						'title': page.title
					});
				}
			});

			// Get the parameters for movies and TV shows
			let genres = PARAMS.get('genres');
			if(genres) {
				genres = genres.split(',');
				genres = '&filter[genre][condition][path]=field_genre.id&filter[genre][condition][operator]=IN&filter[genre][condition][value][]=' + genres.join('&filter[genre][condition][value][]=');
			}

			let short = PARAMS.get('short');
			if(short != null) {
				short = '&filter[field_is_short]=' + short;
			} else {
				short = '';
			}

			let indie = PARAMS.get('indie');
			if(indie != null) {
				indie = '&filter[field_is_indie]=' + indie;
			} else {
				indie = '';
			}

			let sort = PARAMS.get('sort');
			if(!sort) {
				sort = 'title';
			}

			// Display page specific content
			if(PATH.includes('/movies')) {
				let release = PARAMS.get('release_date');
				if(release != null) {
					let from = release.split(',')[0];
					let to = release.split(',')[1];
					
					release = '&filter[from][condition][value]=' + from + '&filter[from][condition][path]=field_release_date&filter[from][condition][operator]=>&filter[to][condition][value]=' + to + '&filter[to][condition][path]=field_release_date&filter[to][condition][operator]=<'
				} else {
					release = ''; // Value mustn't be null, otherwise Drupal's JSON:API won't return anything
				}

				displayFilms('movie', config.entriesPerPage, 0, genres + release + short + indie, sort)

				// Display controls
				let contentElement = document.getElementById('site-content');
				let controlsContainer = document.createElement('div');
				controlsContainer.setAttribute('id', 'controls');
				let contentRow = contentElement.getElementsByClassName('row')[0];
				contentElement.insertBefore(controlsContainer, contentRow);

				displayControls();
				addBodyClasses(['movies']);
			} else if(PATH == '/tv-shows') {
				let status = PARAMS.get('status');
				if(status != null) {
					status = '&filter[field_status]=' + status;
				} else {
					status = '';
				}

				displayFilms('tv_show', config.entriesPerPage, 0, genres + status, sort);

				// Display controls
				let contentElement = document.getElementById('site-content');
				let controlsContainer = document.createElement('div');
				controlsContainer.setAttribute('id', 'controls');
				let contentRow = contentElement.getElementsByClassName('row')[0];
				contentElement.insertBefore(controlsContainer, contentRow);

				displayControls();
				addBodyClasses(['tv-shows']);
			}

			// If there is no title or content, we display an error message
			if((PAGE_TITLE.innerHTML == '') && (PAGE_CONTENT.innerHTML == '')) {
				pageNotFound({ type: 'page' });
			}
		})
		.catch((err) => {
			displayMessage({
				type: 'error',
				message: 'We had problems fetching the content. Please try again later.' + err
			})
		});
}

export { displayPage };
