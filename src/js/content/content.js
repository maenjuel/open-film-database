import { PAGE_TITLE, PAGE_CONTENT, PATH } from '../core/constants.js';
import { addBodyClasses, displayMessage, displayLoader, headerHeight, metaTags, openGraph, pageNotFound, removeLoader } from '../core/global.js';
import { fetchFilms, fetchFilm } from '../components/film.js';
import { fetchTag } from '../components/taxonomy.js';
import { getCountryName, getLanguageName } from '../components/locale.js';
import { mediaPagination } from '../components/pagination.js';
import { displayPage } from './page.js';

function displayContent() {
	if(PATH == '/') {
		PAGE_TITLE.innerHTML = typogr.typogrify('Hello there!');
		PAGE_CONTENT.innerHTML = typogr.typogrify('<p>Welcome to the Open Film Database (OFDb), a free and open database for movies and TV shows. Maintained by the community and published under an open license, the OFDb leaves the control over its data in the hands of the public.</p>');
		PAGE_CONTENT.innerHTML += '<h2>Newest entries</h2><h3>Movies</h3><ul id="movies"></ul><h3>TV shows</h3><ul id="tv-shows"></ul>';
		displayFilms('movie', 8, 0, '', '-created');
		displayFilms('tv_show', 8, 0, '', '-created');
		addBodyClasses(['home']);
	} else if(PATH.includes('/movie/')) {
		displayMovie();
	} else if(PATH.includes('/tv-show/')) {
		displayTVShow();
	} else {
		displayPage();
	}
}

function displayFilms(type, limit, offset, filter, sort) {
	// First we display the loader and make sure the list is empty
	displayLoader();

	let list;
	if(type == 'movie') {
		list = document.getElementById('movies');
	} else {
		list = document.getElementById('tv-shows');
	}
	list.innerHTML = '';

	// Now we add the media to the list
	fetchFilms(type, limit, offset, filter, sort)
		.then(media => {
			// If there are no results, we display a message saying so
			if(!media.errors && media.data.length == 0) {
				PAGE_CONTENT.innerHTML += 'We found no entries matching your filters. ¯\\_(ツ)_/¯';
				removeLoader();
				return;
			}

			// Otherwise we display the data
			let data = media.data;
			let links = media.links;

			data.forEach(film => {
				let id = film.id;
				let title = film.attributes.title;
				let poster = film.attributes.field_poster;
				let release = film.attributes.field_release_date;

				if(poster != null) {
					poster = film.attributes.field_poster.uri.split('File:').pop();
					poster = '<img src="https://en.wikipedia.org/wiki/Special:Redirect/file?wptype=file&wpvalue=' + poster + '" class="poster" title="Poster" />';
				} else {
					poster = '<div class="no-poster"><i class="icon material-icons md-48 md-movie"></i></div>';
				}

				if(type == 'movie') {
					if(release != null) {
						let date = new Date(release);
						release = '<div class="release">' + date.getFullYear() + '</div>';
					} else {
						release = '';
					}

					list.innerHTML += '<li><a href="/movie/' + id + '">' + poster + '<div class="meta"><div class="title">' + typogr.typogrify(title) + '</div>' + release + '</div></a></li>';
				} else {
					list.innerHTML += '<li><a href="/tv-show/' + id + '">' + poster + '<div class="meta"><div class="title">' + typogr.typogrify(title) + '</div></div></a></li>';
				}
			});

			// Add dummy boxes to fix flex items on last page
			list.innerHTML += '<li class="dummy-item"></li><li class="dummy-item"></li><li class="dummy-item"></li><li class="dummy-item"></li><li class="dummy-item"></li><li class="dummy-item"></li><li class="dummy-item"></li>';

			// Add pagination if there is moralmanace than one page
			if(links.prev != undefined || links.next != undefined) {
				// Add pagination container
				let contentElement = document.getElementById('content');
				let pagination = document.getElementById('pagination');

				if(pagination == undefined) {
					pagination = document.createElement('div');
					pagination.setAttribute('id', 'pagination');
					contentElement.parentNode.insertBefore(pagination, contentElement.nextSibling);
				}

				// Add buttons
				let buttons;

				if(links.prev == undefined && links.next != undefined) {
					buttons = 'next';
				} else if (links.prev != undefined && links.next == undefined) {
					buttons = 'prev';
				} else {
					buttons = 'both';
				}

				mediaPagination(type, buttons, offset);
			}

			// remove loader
			removeLoader();
		})
		.catch(() => {
			displayMessage({
				type: 'error',
				message: 'We had problems displaying the content. Please try again later.'
			});
		});
}

function displayMovie() {
	displayLoader(); // Before we start we display the loader

	let id = PATH.split('/movie/').pop(); // Get movie ID from URL
	let inc = ['field_genre', 'field_runtime', 'field_budget', 'field_distributor', 'field_production_company', 'field_aspect_ratio', 'field_colour', 'field_audio_format','field_film_stock']; // The fields we want to include in our JSON request

	// Get container elements
	let posterContainer = document.getElementById('poster');
	let originalContainer = document.getElementById('original-title');
	let runtimeContainer = document.getElementById('runtime');
	let genreContainer = document.getElementById('genre');
	let indieContainer = document.getElementById('independent');
	let budgetContainer = document.getElementById('budget');
	let releaseContainer = document.getElementById('release-date');
	let plotContainer = document.getElementById('plot');
	let countryContainer = document.getElementById('country');
	let languageContainer = document.getElementById('language');
	let websiteContainer = document.getElementById('website');
	let distributorContainer = document.getElementById('distributor');
	let productionContainer = document.getElementById('production-company');
	let aspectContainer = document.getElementById('aspect-ratio');
	let audioContainer = document.getElementById('audio-format');
	let colourContainer = document.getElementById('colour');
	let stockContainer = document.getElementById('film-stock');
	let linksContainer = document.getElementById('links');

	fetchFilm('movie', id, inc)
		.then(movie => {
			// If there is a 404 error, we display an error page and stop the execution of the function
			if(movie.errors && movie.errors[0].status == 404) {
				pageNotFound('movie');
				return;
			}

			// Define data points
			let title = movie.data.attributes.title;
			let poster = movie.data.attributes.field_poster;
			let original = movie.data.attributes.field_original_title;
			let runtime = movie.data.attributes.field_runtime;
			let release = movie.data.attributes.field_release_date;
			let short = movie.data.attributes.field_is_short;
			let indie = movie.data.attributes.field_is_indie;
			let budget = movie.data.relationships.field_budget.data;
			let plot = movie.data.attributes.body;
			let distributor = movie.data.relationships.field_distributor.data;
			let production = movie.data.relationships.field_production_company.data;
			let language = movie.data.attributes.field_language;
			let country = movie.data.attributes.field_country;
			let website = movie.data.attributes.field_website;
			let wikidata = movie.data.attributes.field_wikidata;
			let wikipedia = movie.data.attributes.field_wikipedia;
			let imdb = movie.data.attributes.field_imdb;
			let omdb = movie.data.attributes.field_omdb;
			let thetvdb = movie.data.attributes.field_thetvdb;
			let tmdb = movie.data.attributes.field_tmdb;
			let metacritic = movie.data.attributes.field_metacritic;
			let rottentomatoes = movie.data.attributes.field_rotten_tomatoes;
			let aspect = movie.data.relationships.field_aspect_ratio.data;
			let audio = movie.data.relationships.field_audio_format.data;
			let colour = movie.data.relationships.field_colour.data;
			let stock = movie.data.relationships.field_film_stock.data;

			// First we loop through the included field of the json data and populate
			// The needed data accordingly. We'll use that data to display special content
			let genres = [];
			let distArray = [];
			let prodArray = [];
			let runtimes = [];
			let aspects = [];
			let colours = [];
			let audioArray = [];

			if(movie.included) {
				movie.included.forEach(term => {
					// Get movie genre(s)
					if(term.type == 'taxonomy_term--genre') {
						genres.push(term.attributes.name);
					}

					// Get runtime(s)
					if(term.type == 'paragraph--runtime') {;
						runtimes.push({
							'version': term.relationships.field_version.data.id,
							'runtime': term.attributes.field_runtime
						});
					}

					// Get distributor(s)
					if (distributor.length > 0) {
						distributor.forEach(dist => {
							if(term.id == dist.id) {
								distArray.push(term.attributes.name);
							}
						});
					}

					// Get production company(-ies)
					if (production.length > 0) {
						production.forEach(prod => {
							if(term.id == prod.id) {
								prodArray.push(term.attributes.name);
							}
						});
					}

					// Get aspect ratio(s)
					if (aspect.length > 0) {
						aspect.forEach(asp => {
							if(term.id == asp.id) {
								aspects.push(term.attributes.name);
							}
						});
					}

					// Get colours
					if (colour.length > 0) {
						colour.forEach(col => {
							if(term.id == col.id) {
								colours.push(term.attributes.name);
							}
						});
					}

					// Get audio formats
					if (audio.length > 0) {
						audio.forEach(aud => {
							if(term.id == aud.id) {
								audioArray.push(term.attributes.name);
							}
						});
					}

					// Get film stock
					if (stock) {
						// Since we allow only one value, we don't need a loop
						if(term.id == stock.id) {
							stock = term.attributes.name;
						}
					}
				});
			}

			// Now we populate the data we gained from that loop before
			if(genres.length > 0) {
				genreContainer.classList.remove('empty');
				genreContainer.innerHTML = '<i class="icon material-icons md-18 md-local_offer"></i><ul></ul>';
				let list = genreContainer.getElementsByTagName('ul')[0];
				genres.forEach(genre => {
				list.innerHTML += '<li>' + typogr.typogrify(genre) + '</li>';
				});
			}

			if (distributor.length > 0) {
				distributor.forEach(dist => {
					movie.included.forEach(term => {
						if(term.id == dist.id) {
							distArray.push(term.attributes.name);
						}
					});
				});

				distArray = [...new Set(distArray)]; // In case a company is distributor AND production company, there will be duplicates we remove here

				distributorContainer.innerHTML = '<i class="icon material-icons-outline md-18 md-local_shipping"></i><ul></ul>';
				let list = distributorContainer.getElementsByTagName('ul')[0];
				distArray.forEach(dist => {
					list.innerHTML += '<li>' + typogr.typogrify(dist) + '</li>';
				});
			}

			if (production.length > 0) {
				production.forEach(prod => {
					movie.included.forEach(term => {
						if(term.id == prod.id) {
							prodArray.push(term.attributes.name);
						}
					});
				});

				prodArray = [...new Set(prodArray)]; // In case a company is distributor AND production company, there will be duplicates we remove here

				productionContainer.innerHTML += '<i class="icon material-icons md-18 md-handyman"></i><ul></ul>';
				let list = productionContainer.getElementsByTagName('ul')[0];
				prodArray.forEach(prod => {
					list.innerHTML += '<li>' + typogr.typogrify(prod) + '</li>';
				});
			}

			if(runtimes.length > 0){
				if(runtimes.length > 1) {
					// We want to display the runtime of the initial release, hence we loop through
					// The array to look for the respective release
					let theatrical = runtimes.find(v => v.version === '5fa05e7e-e46d-4993-a0c1-61ab5f29ccbf');
					let streaming = runtimes.find(v => v.version === '41d2bd1d-a7b9-4a65-ae0d-94936570ea65');
					let broadcasting = runtimes.find(v => v.version === '7d3f2265-0c9c-438e-b8f2-d2f506e93cac');

					if(theatrical) {
						// Theatrical release
						runtimeContainer.innerHTML = '<i class="icon material-icons md-18 md-av_timer"></i>' + theatrical.runtime + ' minutes';
					} else if(streaming) {
						// Streaming release
						runtimeContainer.innerHTML = '<i class="icon material-icons md-18 md-av_timer"></i>' + streaming.runtime + ' minutes';
					} else if(broadcasting) {
						// Broadcasting release
						runtimeContainer.innerHTML = '<i class="icon material-icons md-18 md-av_timer"></i>' + broadcasting.runtime + ' minutes';
					} else {
						// Otherwise we just pick the first entry
						runtimeContainer.innerHTML = '<i class="icon material-icons md-18 md-av_timer"></i>' + runtimes[0].runtime + ' minutes';
					}
				} else {
					// If there is only one runtime, we simply dislay that one
					runtimeContainer.innerHTML = '<i class="icon material-icons md-18 md-av_timer"></i>' + runtimes[0].runtime + ' minutes';
				}

				if(short) {
					runtimeContainer.innerHTML += ' (short film)';
				}

				runtimeContainer.classList.remove('empty');
			}

			if(budget){
				let included = movie.included;

				included.forEach(inc => {
					if(inc.type === 'paragraph--budget'){
						let amount = inc.attributes.field_amount;
						let currencyId = inc.relationships.field_currency.data.id;

						fetchTag('currency', currencyId)
							.then(tag => {
								let currencyCode = tag.data.attributes.name;
								budgetContainer.innerHTML += '<i class="icon material-icons-outline md-18 md-savings"></i>' + Number(amount).toLocaleString('en-GB')  + '&nbsp;' + currencyCode;
								budgetContainer.classList.remove('empty');

								// remove loader
								removeLoader();
							});
					}
				});
			}

			// Display technical aspects, if available
			if(aspect.length > 0 || colour.length > 0 || stock || audio.length > 0) {
				if(aspect.length > 0) {
					aspectContainer.innerHTML += '<i class="icon material-icons md-18 md-aspect_ratio"></i><ul></ul>';

					let list = aspectContainer.getElementsByTagName('ul')[0];
					aspects.forEach(asp => {
						list.innerHTML += '<li>' + typogr.typogrify(asp) + '</li>';
					});
				}

				if(colour.length > 0) {
					colourContainer.innerHTML += '<i class="icon material-icons-outline md-18 md-palette"></i><ul></ul>';

					let list = colourContainer.getElementsByTagName('ul')[0];
					colours.forEach(col => {
						list.innerHTML += '<li>' + typogr.typogrify(col) + '</li>';
					});
				}

				if(audio.length > 0) {
					audioContainer.innerHTML += '<i class="icon material-icons-outline md-18 md-speaker"></i><ul></ul>';

					let list = audioContainer.getElementsByTagName('ul')[0];
					audioArray.forEach(aud => {
						list.innerHTML += '<li>' + typogr.typogrify(aud) + '</li>';
					});
				}

				if(stock) {
					stockContainer.innerHTML += '<i class="icon material-icons-outline md-18 md-theaters"></i>' + stock;
				}

				// Remove empty class so the element has proper margin
				let technicalContainer = document.getElementById('technical');
				technicalContainer.classList.remove('empty');
			}

			// Now follows the other data
			if(indie) {
				indieContainer.innerHTML += '<i class="icon material-icons-outline md-18 md-rocket_launch"></i>Independent';
				indieContainer.classList.remove('empty');
			}

			if(poster != null) {
				poster = movie.data.attributes.field_poster.uri.split('File:').pop();
				posterContainer.innerHTML = '<img src="https://en.wikipedia.org/wiki/Special:Redirect/file?wptype=file&wpvalue=' + poster + '" title="Movie poster" />';
			}

			if(plot) {
				plot = plot.value;
			} else {
				plot = 'We don\'t have a plot for this movie';
			}

			if(original) {
				originalContainer.innerHTML = '<i class="icon material-icons md-18 md-language"></i>' + typogr.typogrify(original);
				originalContainer.classList.remove('empty');
			}

			if(release) {
				release = new Date(release);

				let day;
				let month;

				if(release.getDate() < 10) {
					day = '0' + release.getDate();
				} else {
					day = release.getDate();
				}

				if((release.getMonth() + 1) < 10) {
					month = '0' + (release.getMonth() + 1);
				} else {
					month = release.getMonth() + 1;
				}
				releaseContainer.innerHTML = '<i class="icon material-icons md-18 md-today"></i>' + day + '.' + month + '.' + release.getFullYear();
			}

			if(country.length > 0) {
				countryContainer.innerHTML = '<i class="icon material-icons md-18 md-public"></i><ul></ul>';
				let list = countryContainer.getElementsByTagName('ul')[0];
				let countryData = '../vendor/benkeen/country-region-data/data.json';

				for (let i = 0; i < country.length; i++) {
					getCountryName(country[i])
						.then(res => {
							list.innerHTML += '<li>' + typogr.typogrify(res) + '</li>';
						})
						.catch(() => {
							displayMessage({
								type: 'error',
								message: 'There was a problem displaying the country name.'
							})
						});
				}
			}

			if(language.length > 0) {
				languageContainer.innerHTML = '<i class="icon material-icons md-18 md-translate"></i><ul></ul>';
				let list = languageContainer.getElementsByTagName('ul')[0];

				language.forEach(lang => {
					lang = lang.substring(0,3); // ISO 639-3 only needs 3 characters
					getLanguageName(lang)
						.then(res => {
							res = res.split(';', 1)[0];
							list.innerHTML += '<li>' + typogr.typogrify(res) + '</li>';
						})
						.catch(() => {
							displayMessage({
								type: 'error',
								message: 'There was a problem displaying the languages.'
							})
						});
				});
			}

			if(website) {
					websiteContainer.innerHTML = '<i class="icon material-icons md-18 md-link"></i><a href="' + website.uri + '" alt="Website" title="Website" target="_blank">Website</a>';
				}

			if(wikipedia||wikidata||imdb||omdb||tmdb||thetvdb||metacritic||rottentomatoes) {
				linksContainer.innerHTML = '<i class="icon material-icons-outline md-18 md-info"></i><ul></ul>';
				let list = linksContainer.getElementsByTagName('ul')[0];

				if(wikipedia) {
					list.innerHTML += '<li><a href="' + wikipedia.uri + '" alt="Wikipedia" title="Wikipedia" target="_blank">Wikipedia</a></li>';
				}

				if(wikidata) {
					list.innerHTML += '<li><a href="https://www.wikidata.org/wiki/' + wikidata + '" alt="Wikidata" title="Wikidata" target="_blank">Wikidata</a></li>';
				}

				if(imdb) {
					list.innerHTML += '<li><a href="https://www.imdb.com/title/' + imdb + '" alt="Internet Movie Database" title="Internet Movie Database" target="_blank">IMDb</a></li>';
				}

				if(omdb) {
					list.innerHTML += '<li><a href="https://www.omdb.org/movie/' + omdb + '" alt="Open Media Database" title="Open Media Database" target="_blank">omdb</a></li>';
				}

				if(tmdb) {
					list.innerHTML += '<li><a href="https://www.themoviedb.org/movie/' + tmdb + '" alt="The Movie Database" title="The Movie Database" target="_blank">TMDb</a></li>';
				}

				if(thetvdb) {
					list.innerHTML += '<li><a href="https://www.thetvdb.com/dereferrer/movie/' + thetvdb + '" alt="TheTVDb.com" title="TheTVDb.com" target="_blank">TheTVDb.com</a></li>';
				}

				if(metacritic) {
					list.innerHTML += '<li><a href="' + metacritic.uri + '" alt="Metacritic" title="Metacritic" target="_blank">Metacritic</a></li>';
				}

				if(rottentomatoes) {
					list.innerHTML += '<li><a href="' + rottentomatoes.uri + '" alt="Rotten Tomatoes" title="Rotten Tomatoes" target="_blank">Rotten Tomatoes</a></li>';
				}
			}

			PAGE_TITLE.innerHTML = typogr.typogrify(title);
			plotContainer.innerHTML = '<p>' + typogr.typogrify(plot) + '</p>';

			// Set the correct meta tags
			let keywords = [];
			if(original) {
				keywords = [title.replace(/"/gm, '\''), original.replace(/"/gm, '\'')]
			} else {
				keywords = [title.replace(/"/gm, '\'')]
			}

			// Remove loader if we don't have a budget (otherwise it was removed there)
			if(!budget) {
				removeLoader();
			}

			metaTags({
				'title': title.replace(/"/gm, '\''),
				'description': plot.replace(/"/gm, '\''),
				'keywords': keywords
			})

			// Set the correct open graphs
			openGraph({
				'title': title.replace(/"/gm, '\''),
				'description': plot.replace(/"/gm, '\''),
				'type': 'video.movie'
			})
		})
		.catch(() => {
			displayMessage({
				type: 'error',
				message: 'We had problems displaying the content. Please try again later.'
			})
		});
}

function displayTVShow() {
	let id = PATH.split('/tv-show/').pop(); // Get movie ID from URL
	let inc = ['field_genre', 'field_network', 'field_production_company', 'field_episodes', 'field_aspect_ratio', 'field_colour', 'field_audio_format']; // The fields we want to include in our JSON request

	// Get container elements
	let posterContainer = document.getElementById('poster');
	let originalContainer = document.getElementById('original-title');
	let genreContainer = document.getElementById('genre');
	let statusContainer = document.getElementById('status');
	let releaseContainer = document.getElementById('release-date');
	let plotContainer = document.getElementById('plot');
	let countryContainer = document.getElementById('country');
	let languageContainer = document.getElementById('language');
	let websiteContainer = document.getElementById('website');
	let networkContainer = document.getElementById('network');
	let productionContainer = document.getElementById('production-company');
	let aspectContainer = document.getElementById('aspect-ratio');
	let audioContainer = document.getElementById('audio-format');
	let colourContainer = document.getElementById('colour');
	let linksContainer = document.getElementById('links');

	fetchFilm('tv_show', id, inc)
		.then(show => {
			// If there is a 404 error, we display an error page and stop the execution of the function
			if(show.errors && show.errors[0].status == 404) {
				pageNotFound('tv_show');
				return;
			}

			// Define data points
			let title = show.data.attributes.title;
			let poster = show.data.attributes.field_poster;
			let original = show.data.attributes.field_original_title;
			let release = show.data.attributes.field_release_date;
			let status = show.data.attributes.field_status;
			let plot = show.data.attributes.body;
			let network = show.data.relationships.field_network.data;
			let production = show.data.relationships.field_production_company.data;
			let language = show.data.attributes.field_language;
			let country = show.data.attributes.field_country;
			let website = show.data.attributes.field_website;
			let wikidata = show.data.attributes.field_wikidata;
			let wikipedia = show.data.attributes.field_wikipedia;
			let imdb = show.data.attributes.field_imdb;
			let omdb = show.data.attributes.field_omdb;
			let thetvdb = show.data.attributes.field_thetvdb;
			let tmdb = show.data.attributes.field_tmdb;
			let metacritic = show.data.attributes.field_metacritic;
			let rottentomatoes = show.data.attributes.field_rotten_tomatoes;
			let aspect = show.data.relationships.field_aspect_ratio.data;
			let audio = show.data.relationships.field_audio_format.data;
			let colour = show.data.relationships.field_colour.data;

			// First we loop through the included field of the json data and populate
			// The needed data accordingly. We'll use that data to display special content
			let genres = [];
			let netArray = [];
			let prodArray = [];
			let episodes = [];
			let aspects = [];
			let colours = [];
			let audioArray = [];

			if(show.included) {
				show.included.forEach(term => {
					// Get TV show genre(s)
					if(term.type == 'taxonomy_term--genre') {
						genres.push(term.attributes.name);
					}

					// Get network(s)
					if (network.length > 0) {
						network.forEach(dist => {
							if(term.id == dist.id) {
								netArray.push(term.attributes.name);
							}
						});
					}

					// Get production company(-ies)
					if (production.length > 0) {
						production.forEach(prod => {
							if(term.id == prod.id) {
								prodArray.push(term.attributes.name);
							}
						});
					}

					// Get aspect ratio(s)
					if (aspect.length > 0) {
						aspect.forEach(asp => {
							if(term.id == asp.id) {
								aspects.push(term.attributes.name);
							}
						});
					}

					// Get colours
					if (colour.length > 0) {
						colour.forEach(col => {
							if(term.id == col.id) {
								colours.push(term.attributes.name);
							}
						});
					}

					// Get audio formats
					if (audio.length > 0) {
						audio.forEach(aud => {
							if(term.id == aud.id) {
								audioArray.push(term.attributes.name);
							}
						});
					}

					// Get episodes
					if(term.type == 'paragraph--episode' && term.relationships.field_episode_title.data != null) {
						let episode = {
							season: term.attributes.field_season,
							episode: term.attributes.field_episode_number,
							id: term.relationships.field_episode_title.data.id
						};

						episodes.push(episode);
					}
				});
			}

			// Now we populate the data we gained from that loop before
			if(genres.length > 0) {
				genreContainer.classList.remove('empty');
				genreContainer.innerHTML = '<i class="icon material-icons md-18 md-local_offer"></i><ul></ul>';
				let list = genreContainer.getElementsByTagName('ul')[0];
				genres.forEach(genre => {
				list.innerHTML += '<li>' + typogr.typogrify(genre) + '</li>';
				});
			}

			if (network.length > 0) {
				network.forEach(net => {
					show.included.forEach(term => {
						if(term.id == net.id) {
							netArray.push(term.attributes.name);
						}
					});
				});

				netArray = [...new Set(netArray)]; // In case a company is network AND production company, there will be duplicates we remove here

				networkContainer.innerHTML = '<i class="icon material-icons md-18 md-tv"></i><ul></ul>';
				let list = networkContainer.getElementsByTagName('ul')[0];
				netArray.forEach(dist => {
					list.innerHTML += '<li>' + typogr.typogrify(dist) + '</li>';
				});
			}

			if (production.length > 0) {
				production.forEach(prod => {
					show.included.forEach(term => {
						if(term.id == prod.id) {
							prodArray.push(term.attributes.name);
						}
					});
				});

				prodArray = [...new Set(prodArray)]; // In case a company is network AND production company, there will be duplicates we remove here

				productionContainer.innerHTML += '<i class="icon material-icons md-18 md-handyman"></i><ul></ul>';
				let list = productionContainer.getElementsByTagName('ul')[0];
				prodArray.forEach(prod => {
					list.innerHTML += '<li>' + typogr.typogrify(prod) + '</li>';
				});
			}

			// Display technical aspects, if available
			if(aspect.length > 0 || colour.length > 0 || audio.length > 0) {
				if(aspect.length > 0) {
					aspectContainer.innerHTML += '<i class="icon material-icons md-18 md-aspect_ratio"></i><ul></ul>';

					let list = aspectContainer.getElementsByTagName('ul')[0];
					aspects.forEach(asp => {
						list.innerHTML += '<li>' + typogr.typogrify(asp) + '</li>';
					});
				}

				if(colour.length > 0) {
					colourContainer.innerHTML += '<i class="icon material-icons-outline md-18 md-palette"></i><ul></ul>';

					let list = colourContainer.getElementsByTagName('ul')[0];
					colours.forEach(col => {
						list.innerHTML += '<li>' + typogr.typogrify(col) + '</li>';
					});
				}

				if(audio.length > 0) {
					audioContainer.innerHTML += '<i class="icon material-icons-outline md-18 md-speaker"></i><ul></ul>';

					let list = audioContainer.getElementsByTagName('ul')[0];
					audioArray.forEach(aud => {
						list.innerHTML += '<li>' + typogr.typogrify(aud) + '</li>';
					});
				}

				// Remove empty class so the element has proper margin
				let technicalContainer = document.getElementById('technical');
				technicalContainer.classList.remove('empty');
			}

			// Now follows the other data
			if(poster != null) {
				poster = show.data.attributes.field_poster.uri.split('File:').pop();
				posterContainer.innerHTML = '<img src="https://en.wikipedia.org/wiki/Special:Redirect/file?wptype=file&wpvalue=' + poster + '" title="TV show poster" />';
			}

			if(plot) {
				plot = plot.value;
			} else {
				plot = 'We don\'t have a plot for this TV show';
			}

			if(original) {
				originalContainer.innerHTML = '<i class="icon material-icons md-18 md-language"></i>' + original;
				originalContainer.classList.remove('empty');
			}

			if(status) {
				statusContainer.innerHTML = '<i class="icon material-icons md-18 md-cast"></i>' + status;
				statusContainer.classList.remove('empty');
			}

			if(country.length > 0) {
				countryContainer.innerHTML = '<i class="icon material-icons md-18 md-public"></i><ul></ul>';
				let list = countryContainer.getElementsByTagName('ul')[0];
				let countryData = '../vendor/benkeen/country-region-data/data.json';

				for (let i = 0; i < country.length; i++) {
					getCountryName(country[i])
						.then(res => {
							list.innerHTML += '<li>' + typogr.typogrify(res) + '</li>';
						})
						.catch(() => {
							displayMessage({
								type: 'error',
								message: 'There was a problem displaying the country name.'
							})
						});
				}
			}

			if(language.length > 0) {
				languageContainer.innerHTML = '<i class="icon material-icons md-18 md-translate"></i><ul></ul>';
				let list = languageContainer.getElementsByTagName('ul')[0];

				language.forEach(lang => {
					lang = lang.substring(0,3); // ISO 639-3 only needs 3 characters
					getLanguageName(lang)
						.then(res => {
							res = res.split(';', 1)[0];
							list.innerHTML += '<li>' + typogr.typogrify(res) + '</li>';
						})
						.catch(() => {
							displayMessage({
								type: 'error',
								message: 'There was a problem displaying the languages.'
							})
						});
				});
			}

			if(website) {
					websiteContainer.innerHTML = '<i class="icon material-icons md-18 md-link"></i><a href="' + website.uri + '" alt="Website" title="Website" target="_blank">Website</a>';
				}

			if(wikipedia||wikidata||imdb||omdb||tmdb||thetvdb||metacritic||rottentomatoes) {
				linksContainer.innerHTML = '<i class="icon material-icons-outline md-18 md-info"></i><ul></ul>';
				let list = linksContainer.getElementsByTagName('ul')[0];

				if(wikipedia) {
					list.innerHTML += '<li><a href="' + wikipedia.uri + '" alt="Wikipedia" title="Wikipedia" target="_blank">Wikipedia</a></li>';
				}

				if(wikidata) {
					list.innerHTML += '<li><a href="https://www.wikidata.org/wiki/' + wikidata + '" alt="Wikidata" title="Wikidata" target="_blank">Wikidata</a></li>';
				}

				if(imdb) {
					list.innerHTML += '<li><a href="https://www.imdb.com/title/' + imdb + '" alt="Internet Movie Database" title="Internet Movie Database" target="_blank">IMDb</a></li>';
				}

				if(omdb) {
					list.innerHTML += '<li><a href="https://www.omdb.org/movie/' + omdb + '" alt="Open Media Database" title="Open Media Database" target="_blank">omdb</a></li>';
				}

				if(tmdb) {
					list.innerHTML += '<li><a href="https://www.themoviedb.org/tv/' + tmdb + '" alt="The Movie Database" title="The Movie Database" target="_blank">TMDb</a></li>';
				}

				if(thetvdb) {
					list.innerHTML += '<li><a href="https://www.thetvdb.com/dereferrer/series/' + thetvdb + '" alt="TheTVDb.com" title="TheTVDb.com" target="_blank">TheTVDb.com</a></li>';
				}

				if(metacritic) {
					list.innerHTML += '<li><a href="' + metacritic.uri + '" alt="Metacritic" title="Metacritic" target="_blank">Metacritic</a></li>';
				}

				if(rottentomatoes) {
					list.innerHTML += '<li><a href="' + rottentomatoes.uri + '" alt="Rotten Tomatoes" title="Rotten Tomatoes" target="_blank">Rotten Tomatoes</a></li>';
				}
			}

			// Get episodes
			if(episodes.length > 0) {
				// First we start with an empty array, set a heading and a table
				let episodeList = [];
				let episodesContainer = document.getElementById('episodes');
				episodesContainer.innerHTML += '<h2>Episodes</h2>';
				episodesContainer.innerHTML += '<table><thead><tr><th><abbr title="Season">S</abbr>/<abbr title="Episode">E</abbr></th><th>Description</th><th>Aired</th><th>Runtime</th></tr></thead><tbody></tbody></table>';
				displayLoader('episodes');
				let table = episodesContainer.getElementsByTagName('table')[0];
				let thead = table.getElementsByTagName('thead')[0];
				let tbody = table.getElementsByTagName('tbody')[0];
				thead.children[0].style.top = headerHeight() + 'px';

				// Now we populate the table
				episodes.forEach(ep => {
					let number = episodes.indexOf(ep) + 1; // We'll need that later for the proper order (due to specials with season nr 0)

					fetchFilm('episode', ep.id)
						.then(res => {
							let data = res.data;
							let title = data.attributes.title;
							let original = data.attributes.field_original_title;
							let plot = data.attributes.body;
							let release = data.attributes.field_release_date;
							let runtime = data.attributes.field_episode_runtime;

							if(plot) {
								plot = plot.value;
							}

							let extEp = {
								'number': number,
								'season': ep.season,
								'episode': ep.episode,
								'title': title,
								'original': original,
								'plot': plot,
								'release': release,
								'runtime': runtime
							}

							episodeList.push(extEp);
							if(episodeList.length == episodes.length) {
								episodeList.sort((a, b) => a.number - b.number);

								episodeList.forEach(ep => {
									let season = ep.season;
									let episode = ep.episode;
									let title = ep.title;
									let original = ep.original;
									let plot = ep.plot;
									let release = ep.release;
									let runtime = ep.runtime;

									if(season < 10) {
										season = '0' + season;
									}

									if(episode < 10) {
										episode = '0' + episode;
									}

									if(original) {
										title = typogr.typogrify('<span class="title">' + title + '</span> (' + original + ')');
									} else {
										title = typogr.typogrify('<span class="title">' + title + '</span>');
									}

									if (plot) {
										plot = typogr.typogrify(plot);
									} else {
										plot = '';
									}

									if(release) {
										release = new Date(release);
										let day;
										let month;

										if(release.getDate() < 10) {
											day = '0' + release.getDate();
										} else {
											day = release.getDate();
										}

										if((release.getMonth() + 1) < 10) {
											month = '0' + (release.getMonth() + 1);
										} else {
											month = release.getMonth() + 1;
										}

										release = day + '.' + month + '.' + release.getFullYear();
									} else {
										release = '';
									}

									if(runtime) {
										runtime = runtime + '&nbsp;minutes';
									} else {
										runtime = '';
									}

									tbody.innerHTML += '<tr><td>S' + season + '&nbsp;E' + episode + '</td><td>' + title + '<br />' + plot + '</td><td>' + release + '</td><td>' + runtime + '</td></tr>';
								});

								removeLoader();
							}
						})
						.catch(() => {
							displayMessage({
								type: 'error',
								message: 'There was a problem displaying the episodes. Please try again later.'
							})
						})
				});
			}

			PAGE_TITLE.innerHTML = typogr.typogrify(title);
			plotContainer.innerHTML = '<p>' + typogr.typogrify(plot) + '</p>';

			// Set the correct meta tags
			let keywords = [];
			if(original) {
				keywords = [title.replace(/"/gm, '\''), original.replace(/"/gm, '\'')]
			} else {
				keywords = [title.replace(/"/gm, '\'')]
			}

			metaTags({
				'title': title.replace(/"/gm, '\''),
				'description': plot.replace(/"/gm, '\''),
				'keywords': keywords
			})

			// Set the correct open graphs
			openGraph({
				'title': title.replace(/"/gm, '\''),
				'description': plot.replace(/"/gm, '\''),
				'type': 'video.tv_show'
			})
		})
		.catch(() => {
			displayMessage({
				type: 'error',
				message: 'We had a problem displaying the content. Please try again later.'
			});
		});
}

export { displayContent, displayFilms };
