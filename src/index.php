<!doctype html>
<html class="no-js" lang="en-GB">

<head>
	<title>Open Film Database</title>
	<meta charset="UTF-8">
	<meta name="description" content="The Open Film Database a free and open database for movies and TV shows. It is a project maintained by its community.">
	<meta name="keywords" content="Open Film Database, OFDb, Film, Movie, TV Show, TV Series">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="theme-color" content="#2e3440">

	<meta property="og:site_name" content="Open Film Database" />
	<meta property="og:title" content="Open Film Database" />
	<meta property="og:description" content="The Open Film Database a free and open database for movies and TV shows. It is a project maintained by its community." />
	<meta property="og:type" content="website" />
	<meta property="og:locale" content="en_GB" />
	<meta property="og:url" content="https://ofdb.cc" />

	<link rel="stylesheet" href="/vendor/google/@material-icons/css/all.css" />
	<link rel="stylesheet" href="/css/ofdb.min.css" />

	<!-- Matomo -->
	<script type="text/javascript">
		// @license magnet:?xt=urn:btih:1f739d935676111cfff4b4693e3816e664797050&dn=gpl-3.0.txt GPL-v3-or-Later
		var _paq = window._paq = window._paq || [];
		/* tracker methods like "setCustomDimension" should be called before "trackPageView" */
		_paq.push(['trackPageView']);
		_paq.push(['enableLinkTracking']);
		(function() {
			var u="//matomo.ofdb.cc/";
			_paq.push(['setTrackerUrl', u+'matomo.php']);
			_paq.push(['setSiteId', '1']);
			var d=document, g=d.createElement('script'), s=d.getElementsByTagName('script')[0];
			g.type='text/javascript'; g.async=true; g.src=u+'matomo.js'; s.parentNode.insertBefore(g,s);
		})();
		// @license-end
	</script>
	<!-- End Matomo Code -->
</head>

<body>
	<!--[if IE]>
		<p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="https://browsehappy.com/">upgrade your browser</a> to improve your experience and security.</p>
	<![endif]-->

	<div id="wrapper">
		<header id="site-header">
			<div class="row">
				<span id="menu-toggler">
					<i class="icon material-icons md-36 md-menu"></i>
				</span> <!-- /#menu-toggler -->

				<div id="site-logo">
					<a href="/" title="Home">Open Film Database</a>
				</div> <!-- /#site-logo -->

				<div id="search">
					<div id="searchbox">
						<input type="text" placeholder="Search movie or TV show..." />
						<i class="icon material-icons md-24 md-search"></i>
					</div> <!-- /#searchbox -->
					<div id="suggestions">
					</div> <!-- /#suggestions -->
				</div> <!-- /#search -->
				<nav id="main-menu">
					<ul>
					</ul>
				</nav> <!-- /#main-menu -->
			</div> <!-- /.row -->
		</header> <!-- /#header -->

		<main id="site-content">
			<div class="row">
				<div id="messages">
				</div> <!-- /#messages -->

				<?php
					// Depending on the URL, we require a different layout. The content itself
					// Will be populated by JavaScript
					$path = $_SERVER['REQUEST_URI'];

					if(strpos($path, 'movie/') !== false) {
						require('assets/layout/movie.php');
					} else if(strpos($path, 'tv-show/') !== false) {
						require('assets/layout/tv-show.php');
					} else {
						require('assets/layout/page.php');
					}
				?>
			</div> <!-- /.row -->
		</main> <!-- /#site-content -->

		<footer id="site-footer">
			<div class="row">
				<nav id="footer-menu">
					<ul>
						<li><a href="/javascript-licenses.php" rel="jslicense" alt="JavaScript licenses" title="JavaScript licenses">JavaScript licenses</a></li>
						<li><a href="https://gitlab.com/maenjuel/open-film-database" alt="Source code" title="Source code" target="_blank">Source code</a></li>
						<li><a href="https://blog.ofdb.cc" alt="Blog" title="Blog" target="_blank">Blog</a></li>
					</ul>
				</nav> <!-- /#footer-menu -->
			</div> <!-- /.row -->
		</footer> <!-- /#site-footer -->
	</div> <!-- /#wrapper -->
	<div id="loader-wrapper"></div>
	<script src="/vendor/ekalinin/typogr.js/typogr.min.js"></script>
	<script src="/js/ofdb.min.js"></script>
</body>

</html>
