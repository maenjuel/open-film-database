<!-- begin content -->
<div id="movie">
	<h1 id="title"></h1>
	<div id="poster"></div>
	<div id="meta">
		<div class="empty" id="original-title"></div>
		<div class="empty" id="genre"></div>
		<div class="empty" id="runtime"></div>
		<div class="empty" id="budget"></div>
		<div class="empty" id="independent"></div>
	</div> <!-- /#meta -->
	<div id="content">
		<div id="plot"></div>
		<div id="details">
			<div id="general">
				<div id="country"></div>
				<div id="language"></div>
				<div id="release-date"></div>
				<div id="website"></div>
				<div id="production">
					<div id="distributor"></div>
					<div id="production-company"></div>
				</div> <!-- /#production -->
			</div> <!-- /#general -->
			<div class="empty" id="technical">
				<div id="aspect-ratio"></div>
				<div id="colour"></div>
				<div id="audio-format"></div>
				<div id="film-stock"></div>
			</div> <!-- /#technical -->
		</div> <!-- details -->
		<div id="links"></div>
	</div> <!--/#content -->
	<div id="links"></div>
</div>
<!-- end content -->
