In this repository the front-end of the Open Film Database is developed. The OFDb is a free and open database for movies and TV shows.

# Licenses

## OFDb front-end
[GPL v3 or later](LICENSE)

## Libraries
* [country-region-data: MIT](https://github.com/country-regions/country-region-data/blob/master/LICENSE.txt)
* [ISO-639-1: MIT](https://www.npmjs.com/package/iso-639-1)
* [material-icons: Apache-2.0](https://github.com/material-icons/material-icons/blob/master/LICENSE)
* [typogr.js: MIT](https://github.com/ekalinin/typogr.js/blob/master/LICENSE)

## Fonts

[Clear Sans](https://01.org/clear-sans) is licensed under [Apache-2.0](http://www.apache.org/licenses/LICENSE-2.0).

# Further development

The decoupled front-end is not in use anymore - the OFDb front-end is now powered by a "regular" Drupal theme. Hence, this repo is archived.

Development of the Drupal theme happens at [Codeberg](https://codeberg.org/OFDb/drupal-theme).
