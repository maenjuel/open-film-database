'use strict';

const CONTENT = '/assets/content.json';
const PAGE_CONTENT = document.getElementById('content');
const PAGE_TITLE = document.getElementById('title');
const PARAMS = new URLSearchParams(window.location.search);
const PATH = window.location.pathname;
const SEARCHBOX = document.getElementById('searchbox').getElementsByTagName('input')[0];
const SUGGESTIONS = document.getElementById('suggestions');
const URL = window.location.href;

// Add classes to body tag
function addBodyClasses(classes) {
	if(!Array.isArray(classes)) {
		console.log('Classes need to be provided as an Array, will ignore them.');
		classes = [];
	}

	document.body.className += classes;
}

// Display messages
function displayMessage(options) {
	var type = options.type;
	var message = options.message;
	var container = document.getElementById('messages');
	var icon;

	if (options.type == 'error') {
		icon = '<i class="icon left material-icons-outline md-18 md-error"></i>';
	} else if (options.type == 'success') {
		icon = '<i class="icon left materiherzlicheal-icons-outline md-18 md-thumb_up"></i>';
	} else {
		icon = '<i class="icon left material-icons-outline md-18 md-info"></i>'; // Fallback icon
		type = 'info';
	}

	if(options.message != undefined) {
		message = options.message;
	} else {
		message = 'We encountered an error, please try again.';
	}

	container.classList.add('show');
	container.classList.add(type); // Add class with the name of the message type
	container.innerHTML = icon + '<span>' + typogr.typogrify(message) + '</span>';
}

// Get header height
function headerHeight() {
	let headerElement = document.getElementById('site-header');
	return headerElement.offsetHeight;
}

// Define breakpoints
let windowWidth = window.innerWidth;

function isMobile() {
	if(windowWidth < 960) {
		return true;
	} else {
		return false;
	}
}

function displayLoader(id) {
	let el = document.getElementById(id);
	let loader = document.getElementById('loader');

	if(!loader) { // We don't want multiple loaders on the screen
		if(el) {
			el.innerHTML += '<div id="loader"><svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" style="margin: 1em auto; background: rgba(0, 0, 0, 0) none repeat scroll 0% 0%; display: block; shape-rendering: auto;" width="30px" height="30px" viewBox="0 0 100 100" preserveAspectRatio="xMidYMid"><circle cx="50" cy="50" r="45" stroke-width="8" stroke="#eceff4" stroke-dasharray="70.68583470577035 70.68583470577035" fill="none" stroke-linecap="round"><animateTransform attributeName="transform" type="rotate" repeatCount="indefinite" dur="1.25s" keyTimes="0;1" values="0 50 50;360 50 50"></animateTransform></circle></div>';
		} else {
				let body = document.body;
				let loaderWrapper = document.getElementById('loader-wrapper');

				loaderWrapper.innerHTML += '<div id="loader"><svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" style="margin: ' + headerHeight() / 2 + 'px auto 0 auto; background: rgba(0, 0, 0, 0) none repeat scroll 0% 0%; display: block; shape-rendering: auto;" width="50px" height="50px" viewBox="0 0 100 100" preserveAspectRatio="xMidYMid"><circle cx="50" cy="50" r="45" stroke-width="8" stroke="#eceff4" stroke-dasharray="70.68583470577035 70.68583470577035" fill="none" stroke-linecap="round"><animateTransform attributeName="transform" type="rotate" repeatCount="indefinite" dur="1.25s" keyTimes="0;1" values="0 50 50;360 50 50"></animateTransform></circle></svg></div>';
				loaderWrapper.setAttribute('style', 'display: block');

				// let's make the page non-scrollable while the loader is displayed
				body.style.overflow = 'hidden';
		}
	}
}

function metaTags(metaTags) {
	let title = metaTags.title;
	let description = metaTags.description;
	let keywords = metaTags.keywords;
	document.getElementsByTagName('meta');

	if(metaTags.hasOwnProperty('title') || title != undefined) {
		document.title = title + ' [Open Film Database]';
	}

	if(metaTags.hasOwnProperty('description') || description != undefined) {
		document.querySelector('meta[name="description"]').setAttribute('content', description);
	}

	if(metaTags.hasOwnProperty('keywords') || keywords != undefined) {
		keywords = document.querySelector('meta[name="keywords"]').content + ', ' + keywords.join(', ');
		document.querySelector('meta[name="keywords"]').setAttribute('content', keywords);
	}
}

function openGraph(og) {
	let title = og.title;
	let description = og.description;
	let type = og.type;
	let url = og.url;

	if(og.hasOwnProperty('title') || title != undefined) {
		document.querySelector('meta[property="og:title"]').setAttribute('content', title  + ' [Open Film Database]');
	}

	if(og.hasOwnProperty('description') || description != undefined) {
		document.querySelector('meta[property="og:description"]').setAttribute('content', description);
	}

	if(og.hasOwnProperty('type') || type != undefined) {
		document.querySelector('meta[property="og:type"]').setAttribute('content', type);
	}

	if(og.hasOwnProperty('url') || url != undefined) {
		document.querySelector('meta[property="og:url"]').setAttribute('content', url);
	} else {
		document.querySelector('meta[property="og:url"]').setAttribute('content', 'https://' + location.hostname + PATH);
	}
}

function pageNotFound(type) {
	let title = 'Uh oh...';
	let message;

	if (type == 'movie') {
		message = 'We couldn\'t find the requested movie.';
	} else if (type == 'tv_show') {
		message = 'We couldn\'t find the requested TV show.';
	} else {
		message = 'We couldn\'t find the requested page.';
	}

	PAGE_TITLE.innerHTML = typogr.typogrify(title);
	PAGE_CONTENT.innerHTML = typogr.typogrify('<p>' + message + '</p><p><a href="/">Go back to the home page</a>.</p>');

	metaTags({
		'title': title
	});
}

function removeLoader() {
	let loaderWrapper = document.getElementById('loader-wrapper');
	let loader = document.getElementById('loader');
	if(loader != null) {
		loader.parentNode.removeChild(loader);

		// let's make the page scrollable again
		let body = document.body;
		body.style.overflow = null;
		loaderWrapper.style.removeProperty('display');
	}
}

let mainMenuContainer = document.getElementById('main-menu');
let mainMenu = document.getElementById('main-menu').getElementsByTagName('ul')[0];
let footerMenu = document.getElementById('footer-menu').getElementsByTagName('ul')[0];
let footerMenuItems = []; // We'll populate that later

// Add mobile menu functionalities
function addHamburgerEventListener() {
	let menuToggler = document.getElementById('menu-toggler');
	menuToggler.addEventListener('click', toggleMenu);
}

function toggleMenu() {
	mainMenuContainer.classList.toggle('show');
}

// Populate page menus
function buildMenus() {
	// Populate menus with the links
	fetch(CONTENT)
		.then(res => res.json())
		.then(json => {
			json = json.page;

			json.forEach(page => {
				let path = page.path;
				let name = page.menu.name;
				let position = page.menu.position;
				let rel = page.menu.rel;
				let linkClass;

				if(path == PATH) {
					linkClass = 'class="active"';
				} else {
					linkClass = '';
				}

				if(position == 'header') {
					if(rel != '') {
						rel = 'rel="' + rel + '"';
					}

					mainMenu.innerHTML += '<li><a href="' + path + '"' + linkClass + ' alt="' + name + '"title="' + name + '"' + rel + '>' + name + '</a></li>';
				} else {
					if(rel != '') {
						rel = 'rel="' + rel + '"';
					}

					if(position != undefined) {
						// First we store the menu items in an array and add them later since the footer menu already contains items
						// Like this we have more control over how to add the items to the menu
						let menuItem = '<li><a href="' + path + '"' + linkClass + ' alt="' + name + '"title="' + name + '"' + rel + '>' + name + '</a></li>';
						footerMenuItems.push(menuItem);
					}
				}
			});

			// Add login link
			mainMenu.innerHTML += '<li><a href="https://admin.ofdb.cc/user/login" alt="Login" title="Login" target="_blank">Login</a></li>';

			// Add footer menu items
			footerMenuItems.reverse().forEach(item => {
				footerMenu.insertAdjacentHTML('afterbegin', item);
			});

			// Add event listener for hamburger icon
			addHamburgerEventListener();
		})
		.catch(err => {
			console.log('Error loading menus: ' + err);
		});

	// If we are on the desktop, we need to add the .show class
	if(!isMobile()) {
		mainMenuContainer.classList.add('show');
	}
}

function contentOffset() {
	let contentElement = document.getElementById('site-content');
	contentElement.style.marginTop = headerHeight() + 'px';
}

const config = {
	'api': {
		'ofdbApiUrl': 'https://admin.ofdb.cc/api',
		'oldbRouterUrl': 'https://admin.ofdb.cc/router/translate-path',
		'wikipediaApiUrl': 'https://en.wikipedia.org/w/api.php'
	},
	'entriesPerPage': 48,
	'searchResultsPerType': 5
};

function fetchFilms(type='movie', limit=50, offset=0, filter='', sort='title') {
	let url = config.api.ofdbApiUrl + '/node/' + type + '/?page[limit]=' + limit + '&page[offset]=' + offset + '&filter[status][value]=1' + filter + '&sort=' + sort;

	return fetch(url)
		.then(res => res.json())
		.then(json => {
			return json;
		})
		.catch(err => console.log(err));
}

function fetchFilm(type='movie', id, include) {
	let inc;

	if(include) {
		inc = '&include=' + include.join(',');
	} else {
		inc = '';
	}

	let url = config.api.ofdbApiUrl + '/node/' + type + '/' + id + '/?filter[status][value]=1' + inc;
	return fetch(url)
		.then(res => res.json())
		.then(json => {
			return json;
		})
		.catch(err => console.log(err));
}

function fetchTags(vocabulary='genre', limit=50, offset=0, sort='name', filter) {
	let url = config.api.ofdbApiUrl + '/taxonomy_term/' + vocabulary + '/?page[limit]=' + limit + '&page[offset]=' + offset + '&filter[status][value]=1' + filter + '&sort=' + sort;

	return fetch(url)
		.then(res => res.json())
		.then(json => {
			return json;
		})
		.catch(err => console.log(err));
}

function fetchTag(vocabulary='genre', id) {
	let url;

	if(id) {
		url = config.api.ofdbApiUrl + '/taxonomy_term/' + vocabulary + '/' + id;
	} else {
		url = config.api.ofdbApiUrl + '/taxonomy_term/' + vocabulary;
	}

	return fetch(url)
		.then(res => res.json())
		.then(json => {
			return json;
		})
		.catch(err => console.log(err));
}

function getCountryName(code) {
	let countryData = '../vendor/benkeen/country-region-data/data.json';

	return fetch(countryData)
		.then(res => res.json())
		.then(json => {
			let country = json.filter(c => (c.countryShortCode === code));
			return country[0].countryName;
		})
		.catch(err => { console.log(err); });
}

function getLanguageName(code) {
	let languageData = '../vendor/Datahub/language-codes/data/language-codes-full_json.json';
	code = code.split('-', 1)[0]; // Sometimes the language code looks like 'zh-', so we remove the dash

	return fetch(languageData)
		.then(res => res.json())
		.then(json => {
			let language;

			if(code.length == 3) {
				language = json.filter(lang => (lang['alpha3-b'] === code));
			} else {
				language = json.filter(lang => (lang.alpha2 === code));
			}

			return language[0].English;
		})
		.catch(err => { console.log(err); });
}

function mediaPagination(type, buttons = 'next', offset = 0) {
	let genres = PARAMS.get('genres');
	if(genres) {
		genres = genres.split(',');
		genres = '&filter[genre][condition][path]=field_genre.id&filter[genre][condition][operator]=IN&filter[genre][condition][value][]=' + genres.join('&filter[genre][condition][value][]=');
	}

	let release = PARAMS.get('release_date');
	if(release != null) {
		let from = release.split(',')[0];
		let to = release.split(',')[1];
		
		release = '&filter[from][condition][value]=' + from + '&filter[from][condition][path]=field_release_date&filter[from][condition][operator]=>&filter[to][condition][value]=' + to + '&filter[to][condition][path]=field_release_date&filter[to][condition][operator]=<';
	} else {
		release = ''; // Value mustn't be null, otherwise Drupal's JSON:API won't return anything
	}

	let status = PARAMS.get('status');
	if(status != null) {
		status = '&filter[field_status]=' + status;
	} else {
		status = '';
	}

	let sort = PARAMS.get('sort');
	if(!sort) {
		sort = 'title';
	}


	let container = document.getElementById('pagination');
	container.innerHTML = '<ul></ul>';
	let list = container.getElementsByTagName('ul')[0];

	if(buttons == 'next') {
		list.innerHTML = '<li id="next"><a class="button secondary" onclick="displayFilms(\'' + type + '\', ' + config.entriesPerPage + ', ' + (offset + config.entriesPerPage) + ', \'' + genres + release + status + '\', \'' + sort + '\')">next page &rsaquo;</a></li>';
	} else if(buttons == 'prev') {
		list.innerHTML = '<li id="previous"><a class="button secondary" onclick="displayFilms(\'' + type+ '\', ' + config.entriesPerPage + ', ' + (offset - config.entriesPerPage) + ', \'' + genres + release + status + '\', \'' + sort +  '\')">&lsaquo; previous page</a></li>';
	} else {
		list.innerHTML = '<li id="previous"><a class="button secondary" onclick="displayFilms(\'' + type+ '\', ' + config.entriesPerPage + ', ' + (offset - config.entriesPerPage) + ', \'' + genres + release + status + '\', \'' + sort + '\')">&lsaquo; previous page</a></li><li id="next"><a class="button secondary" onclick="displayFilms(\'' + type+ '\', ' + config.entriesPerPage + ', ' + (offset + config.entriesPerPage) + ', \'' + genres + '\', \'' + sort + '\')">next page &rsaquo;</a></li>';
	}
}

function buildURLParams(type, parameters, url) {

	// Check parameters
	if(!type || !parameters) {
		console.log('No parameters received. Nothing to do.');
		return;
	} else if(!Array.isArray(parameters)) {
		console.log('Parameter must be an array.');
		return;
	} else {
		parameters = parameters.join(',');
	}

	// If no URL parameter is provided, we simply take the currenty URL
	if(!url) {
		url = URL;
	}

	let params = url.split('?')[1];

	// We'll need to see whether the provided type is already in the URL, hence we need all types in
	// An array (since a parameter might be the same as a type we need to know specifically the types)
	let types = [];
	let nrOfTypes = 0;
	let typeIncluded = false;

	if(params) {
		types.push(params.split('=')[0]); // The first parameter directly after "?"
		for(let i = 0; i < params.length; i++) {
			if(params.charAt(i) == '&'){
				nrOfTypes ++;
			}
		}

		for(let i = 0; i < nrOfTypes; i++) {
			let type = params.split('&')[1];
			type = type.split('=')[0];
			types.push(type);
		}

		types.forEach(t => {
			if(t == type) {
				typeIncluded = true;
			}
		});
	}

	// Now we build all parameters
	if(!params) {
		params = '?' + type + '=' + parameters;
	} else if(typeIncluded) {
		let splitParams = params.split(type + '=');
		let typeParams;

		if(splitParams[1] && splitParams[1].includes('&')) {
			let typeLength = splitParams[1].indexOf('&');
			typeParams = splitParams[1].substring(typeLength);
		} else {
			typeParams = '';
		}

		// remove possible duplicates (if value is already in url, and provided as parameter)
		parameters = [...new Set(parameters.split(','))].join(',');

		// Finally we put everything together
		params = '?' + splitParams[0] + type + '=' + parameters + typeParams;
	} else {
		if(params.includes('sort')) {
			// We want sort at the end of the path
			let splitParams = params.split('sort=');

			params = '?' + splitParams[0] + '&' + type + '=' + parameters + '&sort=' + splitParams[1];

			// Because we moved the sort= parameter, but the "&" or is still there, we remove the leftover character
			params = params.replace('?&', '?');
			params = params.replace('&&', '&');
		} else {
			params = '?' + params + '&' + type + '=' + parameters;
		}
	}

	return params;
}

function removeURLParams(type, url) {
	// If no URL parameter is provided, we simply take the currenty URL
	if(!url) {
		url = URL;
	}

	let params = url.split('?')[1];
	let splitParams = params.split(type + '=');
	let typeParams;

	if(splitParams[1] && splitParams[1].includes('&')) {
		let typeLength = splitParams[1].indexOf('&');
		typeParams = splitParams[1].substring(typeLength);
	} else {
		typeParams = '';
	}

	params = '?' + splitParams[0] + typeParams;

	// Remove leftover characters after removal
	params = params.replace('?&', '?');
	params = params.replace('&&', '&');
			
	if(params.endsWith('?') || params.endsWith('&')) {
		params = params.substring(0, params.length - 1);
	}
					
	return params; // Return parameters
}

function toggleFilters() {
	// Toggle class
	let filtersContainer = document.getElementById('filters');
	filtersContainer.classList.toggle('show');

	// Change button text
	let button = document.getElementById('toggle-filters');
	if(button.innerText.includes('Show')) {
		button.innerHTML = 'Hide filters <i class="icon material-icons md-18 md-keyboard_arrow_up"></i>';
	} else {
		button.innerHTML = 'Show filters <i class="icon material-icons md-18 md-keyboard_arrow_down"></i>';
	}
}

function dateFilter() {
	let dateContainer = document.getElementById('filter-date');
	let dateInput = dateContainer.getElementsByTagName('input');
	let messageContainer = document.getElementById('date-message');
	messageContainer.innerHTML = ''; // Start with an empty field

	let from = dateInput[0].value;
	let to = dateInput[1].value;

	if(from && to) {
		from = new Date(dateInput[0].value);
		to = new Date(dateInput[1].value);

		if(from > to) {
			messageContainer.innerHTML = typogr.typogrify('"From" field must be earlier than "to" field.');
		} else {
			function twoDigitDate(date) {
				if(date < 10) {
					date = '0' + date;
				}

				return date;
			}

			from = from.getFullYear() + '-' + twoDigitDate((from.getMonth() + 1)) + '-' + twoDigitDate(from.getDate());
			to = to.getFullYear() + '-' + twoDigitDate((to.getMonth() + 1)) + '-' + twoDigitDate(to.getDate());

			window.location.replace(PATH + buildURLParams('release_date', [from, to]));
			//console.log(PATH, buildURLParams('release_date', [from, to]), PATH + buildURLParams('release_date', [from, to]));
			
		}
	}
}

function displayControls() {
	// Build URL params
	let url = URL;
	let sort = PARAMS.get('sort');

	if(!sort) {
		sort = 'title';
	}

	let urlParams;
	if(!url.includes('?')) {
		url = url + '?';
	} else if(url.includes('sort=')) {
		url = url.split('sort=');

		if(url[1].includes('&')) {
			let sortLength = url[1].indexOf('&');
			urlParams = url[1].substring(sortLength);
		} else {
			urlParams = '';
		}

		url = url[0] + urlParams;
	} else {
		// If there is no "sort" parameter, we need to add a "&" to the URL before appending the sort parameter
		url = url + '&';
	}

	// Get sort order for buttons
	let preTitle;
	let preRelease;
	let arrow; // We'll use that after displaying the links iteselves

	if(sort.includes('-')) {
		preTitle = '';
		preRelease = '';
		arrow = '<i class="icon material-icons md-18 md-keyboard_arrow_down"></i>';
	} else if(sort == 'field_release_date') {
		preTitle = '';
		preRelease = '-';
		arrow = '<i class="icon material-icons md-18 md-keyboard_arrow_up"></i>';
	} else {
		preTitle = '-';
		preRelease = '';
		arrow = '<i class="icon material-icons md-18 md-keyboard_arrow_up"></i>';
	}

	// Display controls
	let controlsContainer = document.getElementById('controls');

	if(PATH.includes('/movies')) {
		controlsContainer.innerHTML += '<div class="row"><button class="secondary" id="toggle-filters">Show filters <i class="icon material-icons md-18 md-keyboard_arrow_down"></i></button><div id="sorting">Sort: <a href="' + url + 'sort=' + preTitle + 'title" id="sort-title">Title</a> | <a href="' + url + 'sort=' + preRelease + 'field_release_date" id="sort-field_release_date">Release date</a></div><div id="filters"><div id="filter-genre"><strong>Genre</strong></div><div id="grouped"><div id="filter-date"><strong>Release date</strong></div><div id="filter-short"><strong>Short film</strong></div><div id="filter-independent"><strong>Independent</strong></div></div>';
	} else {
		controlsContainer.innerHTML += '<div class="row"><button class="secondary" id="toggle-filters">Show filters <i class="icon material-icons md-18 md-keyboard_arrow_down"></i></button><div id="sorting">Sort: <a href="' + url + 'sort=' + preTitle + 'title" id="sort-title">Title</a></div><div id="filters"><div id="filter-genre"><strong>Genre</strong></div><div id="filter-status"><strong>Status</strong></div></div></div>';
	}

	// Add class and appropriate arrow icon to active sort
	let activeId = 'sort-' + sort.split('-').pop();
	let activeLink = document.getElementById(activeId);
	activeLink.innerHTML += arrow;
	activeLink.classList.add('active');

	// Add content offset (due to position: fixed)
	let contentElement = document.getElementById('site-content');
	let contentRow = contentElement.lastElementChild;
	let controlsHeight = controlsContainer.offsetHeight;
	contentRow.style.marginTop = controlsHeight + 'px';

	// Add genre filter
	let genreContainer = document.getElementById('filter-genre');
	genreContainer.innerHTML += '<ul></ul>';
	let genreList = genreContainer.getElementsByTagName('ul')[0];

	fetchTags()
		.then(res => {
			let data = res.data;
			data.forEach(tag => {
				// Build URL
				let genres = PARAMS.get('genres');
				let genreParams = [];

				if(genres != null) {
					genres = genres.replace('?genres=', ''); // We need to remove ?genres= since it's also part of the string
					genreParams = genres.split(',');
				} else {
					genres = ''; // We simply add an empty value so the app doesn't complain about genres = null
				}

				genreParams.push(tag.id);
				let url = PATH + buildURLParams('genres', genreParams);

				// If the genre is already in the URL, we need to remove it again, since we want to toggle them
				if(genres.includes(tag.id) && genres.startsWith(tag.id) && genres.endsWith(tag.id)) {
					url = PATH + removeURLParams('genres');
				} else if(genres.includes(tag.id) && genres.endsWith(tag.id)) {
					url = url.replace(',' + tag.id, '');
				} else if(genres.includes(tag.id)) {
					url = url.replace(tag.id + ',', '');
				}

				// If we remove all genres, but some parameters follow, the URL could possibly look like this: PATH&type=parameters
				// If that is the case, we need to replace the first "&" with a "?"
				if(url.includes('&') && !url.includes('?')) {
					url = url.replace('&', '?');
				}

				// Get correct icon (checked or unchecked)
				let icon;
				if(genres.includes(tag.id)) {
					icon = '<i class="icon material-icons-outline md-18 md-check_box"></i>';
				} else {
					icon = '<i class="icon material-icons-outline md-18 md-check_box_outline_blank"></i>';
				}

				// Add item to list
				if(PATH.includes('/movies')) {
					// For movies we don't want the 'sitcom' and 'reality' genre in the list, as this applies for TV shows only
					if(tag.id != '0d65942f-0a7e-457c-8799-82f07113ec06' && tag.id != '0fe0c0c3-aae1-4d26-9a3f-2affb9044a06') {
						genreList.innerHTML += '<li id="' + tag.id + '"><a href="' + url + '">' + icon + tag.attributes.name + '</a></li>';
					}
				} else {
					genreList.innerHTML += '<li id="' + tag.id + '"><a href="' + url + '">' + icon + tag.attributes.name + '</a></li>';
				}
			});
		})
		.catch(err => console.log(err));

	// Add event listener for the genre list
	genreList.addEventListener('click', function(e) {
		if (e.target && e.target.matches('li')) {
			toggleGenreFilter(e.target.id);
		}
	});

	// Add Release date filter
	let type;
	if(PATH.includes('/movies')) {
		type = 'movie';
	} else {
		type = 'tv_show';
	}

	// Display movie- or TV show specific filters
	if(type == 'movie') {
		// Release date filter
		fetchFilms(type, 1, 0, '&filter[field_release_date][operator]=CONTAINS&filter[field_release_date][value]=-', 'field_release_date')
			.then(json => {
				let releaseParams = PARAMS.get('release_date');
				let releaseDate = json.data[0].attributes.field_release_date;
				let now = new Date();
				let maxDate = now.getFullYear() + '-' + (now.getMonth() + 1) + '-' + now.getDate();
				let dateContainer = document.getElementById('filter-date');

				if(releaseParams) {
					releaseParams = releaseParams.split(',');

					// Build URL for reset button
					let url = URL.split('release_date=');
					let releaseLength = url[1].indexOf('&');

					if(releaseLength != -1) {
						url = url[0] + url[1].substring(releaseLength);

						if(url.includes('?&')) {
							url = url.replace('?&', '?');
						} else if(url.includes('&&')) {
							url = url.replace('&&', '&');
						}
					} else {
						url = url[0];
						if(url.endsWith('?') || url.endsWith('&')) {
							url = url.substring(0, url.length - 1);
						}
					}

					// Display forms and reset button
					dateContainer.innerHTML += '<div><input type="date" min="' + releaseDate + '" max="' + maxDate + '" value="' + releaseParams[0] + '" /> &ndash; <input type="date" min="' + releaseDate + '" max="' + maxDate + '" value="' + releaseParams[1] + '" /><span id="date-message"></span> <a href="' + url + '" class="button secondary">reset</a></div>';
				} else {
					dateContainer.innerHTML += '<div><input type="date" min="' + releaseDate + '" max="' + maxDate + '" placeholder="' + releaseDate + '" /> &ndash; <input type="date" min="' + releaseDate + '" max="' + maxDate + '" placeholder="' + maxDate + '" /><span id="date-message"></span></div>';
				}

				// Add event listener for the release date filter
				let dateInput = dateContainer.getElementsByTagName('input');
				for (let i = 0; i < dateInput.length; i++) {
					dateInput[i].addEventListener('change', dateFilter);
				}
			})
			.catch(err => console.log(err));

			// Short film filter
			let short = PARAMS.get('short');
			let shortContainer = document.getElementById('filter-short');

			if(short == '1') {
				shortContainer.innerHTML += '<br /><a class="active"><i class="icon material-icons-outline md-18 md-radio_button_checked"></i> yes</a><a href="' + PATH + buildURLParams('short', ['0']) + '"><i class="icon material-icons-outline md-18 md-radio_button_unchecked"></i> no</a><a href="' + PATH + removeURLParams('short') + '"><i class="icon material-icons-outline md-18 md-radio_button_unchecked"></i> both</a>';
			} else if(short == '0'){
				shortContainer.innerHTML += '<br /><a href="' + PATH + buildURLParams('short', ['1']) + '"><i class="icon material-icons-outline md-18 md-radio_button_unchecked"></i> yes</a><a class="active"><i class="icon material-icons-outline md-18 md-radio_button_checked"></i> no</a><a href="' + PATH + removeURLParams('short') + '"><i class="icon material-icons-outline md-18 md-radio_button_unchecked"></i> both</a>';
			} else {
				shortContainer.innerHTML += '<br /><a href="' + PATH + buildURLParams('short', ['1']) + '"><i class="icon material-icons-outline md-18 md-radio_button_unchecked"></i> yes</a><a href="' + PATH + buildURLParams('short', ['0']) + '"><i class="icon material-icons-outline md-18 md-radio_button_unchecked"></i> no</a><a class="active"><i class="icon material-icons-outline md-18 md-radio_button_checked"></i> both</a>';
			}

			// Independent filter
			let indie = PARAMS.get('indie');
			let indieContainer = document.getElementById('filter-independent');

			if(indie == '1') {
				indieContainer.innerHTML += '<br /><a class="active"><i class="icon material-icons-outline md-18 md-radio_button_checked"></i> yes</a><a href="' + PATH + buildURLParams('indie', ['0']) + '"><i class="icon material-icons-outline md-18 md-radio_button_unchecked"></i> no</a><a href="' + PATH + removeURLParams('indie') + '"><i class="icon material-icons-outline md-18 md-radio_button_unchecked"></i> both</a>';
			} else if(indie == '0'){
				indieContainer.innerHTML += '<br /><a href="' + PATH + buildURLParams('indie', ['1']) + '"><i class="icon material-icons-outline md-18 md-radio_button_unchecked"></i> yes</a><a class="active"><i class="icon material-icons-outline md-18 md-radio_button_checked"></i> no</a><a href="' + PATH + removeURLParams('indie') + '"><i class="icon material-icons-outline md-18 md-radio_button_unchecked"></i> both</a>';
			} else {
				indieContainer.innerHTML += '<br /><a href="' + PATH + buildURLParams('indie', ['1']) + '"><i class="icon material-icons-outline md-18 md-radio_button_unchecked"></i> yes</a><a href="' + PATH + buildURLParams('indie', ['0']) + '"><i class="icon material-icons-outline md-18 md-radio_button_unchecked"></i> no</a><a class="active"><i class="icon material-icons-outline md-18 md-radio_button_checked"></i> both</a>';
			}
	} else {
		let status = PARAMS.get('status');
		let statusContainer = document.getElementById('filter-status');

		if(status == 'continuing') {
			statusContainer.innerHTML += '<br /><a class="active"><i class="icon material-icons-outline md-18 md-radio_button_checked"></i> continuing</a><a href="' + PATH + buildURLParams('status', ['ended']) + '"><i class="icon material-icons-outline md-18 md-radio_button_unchecked"></i> ended</a><a href="' + PATH + buildURLParams('status', ['cancelled']) + '"><i class="icon material-icons-outline md-18 md-radio_button_unchecked"></i> cancelled</a><a href="' + PATH + removeURLParams('status') + '"><i class="icon material-icons-outline md-18 md-radio_button_unchecked"></i> all</a>';
		} else if(status == 'ended') {
			statusContainer.innerHTML += '<br /><a href="' + PATH + buildURLParams('status', ['continuing']) + '"><i class="icon material-icons-outline md-18 md-radio_button_unchecked"></i> continuing</a><a class="active"><i class="icon material-icons-outline md-18 md-radio_button_checked"></i> ended</a><a href="' + PATH + buildURLParams('status', ['cancelled']) + '"><i class="icon material-icons-outline md-18 md-radio_button_unchecked"></i> cancelled</a><a href="' + PATH + removeURLParams('status') + '"><i class="icon material-icons-outline md-18 md-radio_button_unchecked"></i> all</a>';
		} else if(status == 'cancelled') {
			statusContainer.innerHTML += '<br /><a href="' + PATH + buildURLParams('status', ['continuing']) + '"><i class="icon material-icons-outline md-18 md-radio_button_unchecked"></i> continuing</a><a href="' + PATH + buildURLParams('status', ['ended']) + '"><i class="icon material-icons-outline md-18 md-radio_button_unchecked"></i> ended</a><a class="active"><i class="icon material-icons-outline md-18 md-radio_button_checked"></i> cancelled</a><a href="' + PATH + removeURLParams('status') + '"><i class="icon material-icons-outline md-18 md-radio_button_unchecked"></i> all</a>';
		} else {
			statusContainer.innerHTML += '<br /><a href="' + PATH + buildURLParams('status', ['continuing']) + '"><i class="icon material-icons-outline md-18 md-radio_button_unchecked"></i> continuing</a><a href="' + PATH + buildURLParams('status', ['ended']) + '"><i class="icon material-icons-outline md-18 md-radio_button_unchecked"></i> ended</a><a href="' + PATH + buildURLParams('status', ['cancelled']) + '"><i class="icon material-icons-outline md-18 md-radio_button_unchecked"></i> cancelled</a><a class="active"><i class="icon material-icons-outline md-18 md-radio_button_checked"></i> all</a>';
		}
	}

	// Add event listener for the toggle filter button
	let button = document.getElementById('toggle-filters');
	button.addEventListener('click', toggleFilters);
}

function displayPage() {
	fetch(CONTENT)
		.then(res => res.json())
		.then(json => {
			json = json.page;

			json.forEach(page => {
				// Add the appropriate page content
				if(page.path == PATH) {
					PAGE_TITLE.innerHTML = typogr.typogrify(page.title);
					PAGE_CONTENT.innerHTML = typogr.typogrify(page.content);

					// Set the correct meta tags
					metaTags({
						'title': page.title
					});

					// Set the correct open graphs
					openGraph({
						'title': page.title
					});
				}
			});

			// Get the parameters for movies and TV shows
			let genres = PARAMS.get('genres');
			if(genres) {
				genres = genres.split(',');
				genres = '&filter[genre][condition][path]=field_genre.id&filter[genre][condition][operator]=IN&filter[genre][condition][value][]=' + genres.join('&filter[genre][condition][value][]=');
			}

			let short = PARAMS.get('short');
			if(short != null) {
				short = '&filter[field_is_short]=' + short;
			} else {
				short = '';
			}

			let indie = PARAMS.get('indie');
			if(indie != null) {
				indie = '&filter[field_is_indie]=' + indie;
			} else {
				indie = '';
			}

			let sort = PARAMS.get('sort');
			if(!sort) {
				sort = 'title';
			}

			// Display page specific content
			if(PATH.includes('/movies')) {
				let release = PARAMS.get('release_date');
				if(release != null) {
					let from = release.split(',')[0];
					let to = release.split(',')[1];
					
					release = '&filter[from][condition][value]=' + from + '&filter[from][condition][path]=field_release_date&filter[from][condition][operator]=>&filter[to][condition][value]=' + to + '&filter[to][condition][path]=field_release_date&filter[to][condition][operator]=<';
				} else {
					release = ''; // Value mustn't be null, otherwise Drupal's JSON:API won't return anything
				}

				displayFilms('movie', config.entriesPerPage, 0, genres + release + short + indie, sort);

				// Display controls
				let contentElement = document.getElementById('site-content');
				let controlsContainer = document.createElement('div');
				controlsContainer.setAttribute('id', 'controls');
				let contentRow = contentElement.getElementsByClassName('row')[0];
				contentElement.insertBefore(controlsContainer, contentRow);

				displayControls();
				addBodyClasses(['movies']);
			} else if(PATH == '/tv-shows') {
				let status = PARAMS.get('status');
				if(status != null) {
					status = '&filter[field_status]=' + status;
				} else {
					status = '';
				}

				displayFilms('tv_show', config.entriesPerPage, 0, genres + status, sort);

				// Display controls
				let contentElement = document.getElementById('site-content');
				let controlsContainer = document.createElement('div');
				controlsContainer.setAttribute('id', 'controls');
				let contentRow = contentElement.getElementsByClassName('row')[0];
				contentElement.insertBefore(controlsContainer, contentRow);

				displayControls();
				addBodyClasses(['tv-shows']);
			}

			// If there is no title or content, we display an error message
			if((PAGE_TITLE.innerHTML == '') && (PAGE_CONTENT.innerHTML == '')) {
				pageNotFound({ type: 'page' });
			}
		})
		.catch((err) => {
			displayMessage({
				type: 'error',
				message: 'We had problems fetching the content. Please try again later.' + err
			});
		});
}

function displayContent() {
	if(PATH == '/') {
		PAGE_TITLE.innerHTML = typogr.typogrify('Hello there!');
		PAGE_CONTENT.innerHTML = typogr.typogrify('<p>Welcome to the Open Film Database (OFDb), a free and open database for movies and TV shows. Maintained by the community and published under an open license, the OFDb leaves the control over its data in the hands of the public.</p>');
		PAGE_CONTENT.innerHTML += '<h2>Newest entries</h2><h3>Movies</h3><ul id="movies"></ul><h3>TV shows</h3><ul id="tv-shows"></ul>';
		displayFilms('movie', 8, 0, '', '-created');
		displayFilms('tv_show', 8, 0, '', '-created');
		addBodyClasses(['home']);
	} else if(PATH.includes('/movie/')) {
		displayMovie();
	} else if(PATH.includes('/tv-show/')) {
		displayTVShow();
	} else {
		displayPage();
	}
}

function displayFilms(type, limit, offset, filter, sort) {
	// First we display the loader and make sure the list is empty
	displayLoader();

	let list;
	if(type == 'movie') {
		list = document.getElementById('movies');
	} else {
		list = document.getElementById('tv-shows');
	}
	list.innerHTML = '';

	// Now we add the media to the list
	fetchFilms(type, limit, offset, filter, sort)
		.then(media => {
			// If there are no results, we display a message saying so
			if(!media.errors && media.data.length == 0) {
				PAGE_CONTENT.innerHTML += 'We found no entries matching your filters. ¯\\_(ツ)_/¯';
				removeLoader();
				return;
			}

			// Otherwise we display the data
			let data = media.data;
			let links = media.links;

			data.forEach(film => {
				let id = film.id;
				let title = film.attributes.title;
				let poster = film.attributes.field_poster;
				let release = film.attributes.field_release_date;

				if(poster != null) {
					poster = film.attributes.field_poster.uri.split('File:').pop();
					poster = '<img src="https://en.wikipedia.org/wiki/Special:Redirect/file?wptype=file&wpvalue=' + poster + '" class="poster" title="Poster" />';
				} else {
					poster = '<div class="no-poster"><i class="icon material-icons md-48 md-movie"></i></div>';
				}

				if(type == 'movie') {
					if(release != null) {
						let date = new Date(release);
						release = '<div class="release">' + date.getFullYear() + '</div>';
					} else {
						release = '';
					}

					list.innerHTML += '<li><a href="/movie/' + id + '">' + poster + '<div class="meta"><div class="title">' + typogr.typogrify(title) + '</div>' + release + '</div></a></li>';
				} else {
					list.innerHTML += '<li><a href="/tv-show/' + id + '">' + poster + '<div class="meta"><div class="title">' + typogr.typogrify(title) + '</div></div></a></li>';
				}
			});

			// Add dummy boxes to fix flex items on last page
			list.innerHTML += '<li class="dummy-item"></li><li class="dummy-item"></li><li class="dummy-item"></li><li class="dummy-item"></li><li class="dummy-item"></li><li class="dummy-item"></li><li class="dummy-item"></li>';

			// Add pagination if there is moralmanace than one page
			if(links.prev != undefined || links.next != undefined) {
				// Add pagination container
				let contentElement = document.getElementById('content');
				let pagination = document.getElementById('pagination');

				if(pagination == undefined) {
					pagination = document.createElement('div');
					pagination.setAttribute('id', 'pagination');
					contentElement.parentNode.insertBefore(pagination, contentElement.nextSibling);
				}

				// Add buttons
				let buttons;

				if(links.prev == undefined && links.next != undefined) {
					buttons = 'next';
				} else if (links.prev != undefined && links.next == undefined) {
					buttons = 'prev';
				} else {
					buttons = 'both';
				}

				mediaPagination(type, buttons, offset);
			}

			// remove loader
			removeLoader();
		})
		.catch(() => {
			displayMessage({
				type: 'error',
				message: 'We had problems displaying the content. Please try again later.'
			});
		});
}

function displayMovie() {
	displayLoader(); // Before we start we display the loader

	let id = PATH.split('/movie/').pop(); // Get movie ID from URL
	let inc = ['field_genre', 'field_runtime', 'field_budget', 'field_distributor', 'field_production_company', 'field_aspect_ratio', 'field_colour', 'field_audio_format','field_film_stock']; // The fields we want to include in our JSON request

	// Get container elements
	let posterContainer = document.getElementById('poster');
	let originalContainer = document.getElementById('original-title');
	let runtimeContainer = document.getElementById('runtime');
	let genreContainer = document.getElementById('genre');
	let indieContainer = document.getElementById('independent');
	let budgetContainer = document.getElementById('budget');
	let releaseContainer = document.getElementById('release-date');
	let plotContainer = document.getElementById('plot');
	let countryContainer = document.getElementById('country');
	let languageContainer = document.getElementById('language');
	let websiteContainer = document.getElementById('website');
	let distributorContainer = document.getElementById('distributor');
	let productionContainer = document.getElementById('production-company');
	let aspectContainer = document.getElementById('aspect-ratio');
	let audioContainer = document.getElementById('audio-format');
	let colourContainer = document.getElementById('colour');
	let stockContainer = document.getElementById('film-stock');
	let linksContainer = document.getElementById('links');

	fetchFilm('movie', id, inc)
		.then(movie => {
			// If there is a 404 error, we display an error page and stop the execution of the function
			if(movie.errors && movie.errors[0].status == 404) {
				pageNotFound('movie');
				return;
			}

			// Define data points
			let title = movie.data.attributes.title;
			let poster = movie.data.attributes.field_poster;
			let original = movie.data.attributes.field_original_title;
			movie.data.attributes.field_runtime;
			let release = movie.data.attributes.field_release_date;
			let short = movie.data.attributes.field_is_short;
			let indie = movie.data.attributes.field_is_indie;
			let budget = movie.data.relationships.field_budget.data;
			let plot = movie.data.attributes.body;
			let distributor = movie.data.relationships.field_distributor.data;
			let production = movie.data.relationships.field_production_company.data;
			let language = movie.data.attributes.field_language;
			let country = movie.data.attributes.field_country;
			let website = movie.data.attributes.field_website;
			let wikidata = movie.data.attributes.field_wikidata;
			let wikipedia = movie.data.attributes.field_wikipedia;
			let imdb = movie.data.attributes.field_imdb;
			let omdb = movie.data.attributes.field_omdb;
			let thetvdb = movie.data.attributes.field_thetvdb;
			let tmdb = movie.data.attributes.field_tmdb;
			let metacritic = movie.data.attributes.field_metacritic;
			let rottentomatoes = movie.data.attributes.field_rotten_tomatoes;
			let aspect = movie.data.relationships.field_aspect_ratio.data;
			let audio = movie.data.relationships.field_audio_format.data;
			let colour = movie.data.relationships.field_colour.data;
			let stock = movie.data.relationships.field_film_stock.data;

			// First we loop through the included field of the json data and populate
			// The needed data accordingly. We'll use that data to display special content
			let genres = [];
			let distArray = [];
			let prodArray = [];
			let runtimes = [];
			let aspects = [];
			let colours = [];
			let audioArray = [];

			if(movie.included) {
				movie.included.forEach(term => {
					// Get movie genre(s)
					if(term.type == 'taxonomy_term--genre') {
						genres.push(term.attributes.name);
					}

					// Get runtime(s)
					if(term.type == 'paragraph--runtime') {						runtimes.push({
							'version': term.relationships.field_version.data.id,
							'runtime': term.attributes.field_runtime
						});
					}

					// Get distributor(s)
					if (distributor.length > 0) {
						distributor.forEach(dist => {
							if(term.id == dist.id) {
								distArray.push(term.attributes.name);
							}
						});
					}

					// Get production company(-ies)
					if (production.length > 0) {
						production.forEach(prod => {
							if(term.id == prod.id) {
								prodArray.push(term.attributes.name);
							}
						});
					}

					// Get aspect ratio(s)
					if (aspect.length > 0) {
						aspect.forEach(asp => {
							if(term.id == asp.id) {
								aspects.push(term.attributes.name);
							}
						});
					}

					// Get colours
					if (colour.length > 0) {
						colour.forEach(col => {
							if(term.id == col.id) {
								colours.push(term.attributes.name);
							}
						});
					}

					// Get audio formats
					if (audio.length > 0) {
						audio.forEach(aud => {
							if(term.id == aud.id) {
								audioArray.push(term.attributes.name);
							}
						});
					}

					// Get film stock
					if (stock) {
						// Since we allow only one value, we don't need a loop
						if(term.id == stock.id) {
							stock = term.attributes.name;
						}
					}
				});
			}

			// Now we populate the data we gained from that loop before
			if(genres.length > 0) {
				genreContainer.classList.remove('empty');
				genreContainer.innerHTML = '<i class="icon material-icons md-18 md-local_offer"></i><ul></ul>';
				let list = genreContainer.getElementsByTagName('ul')[0];
				genres.forEach(genre => {
				list.innerHTML += '<li>' + typogr.typogrify(genre) + '</li>';
				});
			}

			if (distributor.length > 0) {
				distributor.forEach(dist => {
					movie.included.forEach(term => {
						if(term.id == dist.id) {
							distArray.push(term.attributes.name);
						}
					});
				});

				distArray = [...new Set(distArray)]; // In case a company is distributor AND production company, there will be duplicates we remove here

				distributorContainer.innerHTML = '<i class="icon material-icons-outline md-18 md-local_shipping"></i><ul></ul>';
				let list = distributorContainer.getElementsByTagName('ul')[0];
				distArray.forEach(dist => {
					list.innerHTML += '<li>' + typogr.typogrify(dist) + '</li>';
				});
			}

			if (production.length > 0) {
				production.forEach(prod => {
					movie.included.forEach(term => {
						if(term.id == prod.id) {
							prodArray.push(term.attributes.name);
						}
					});
				});

				prodArray = [...new Set(prodArray)]; // In case a company is distributor AND production company, there will be duplicates we remove here

				productionContainer.innerHTML += '<i class="icon material-icons md-18 md-handyman"></i><ul></ul>';
				let list = productionContainer.getElementsByTagName('ul')[0];
				prodArray.forEach(prod => {
					list.innerHTML += '<li>' + typogr.typogrify(prod) + '</li>';
				});
			}

			if(runtimes.length > 0){
				if(runtimes.length > 1) {
					// We want to display the runtime of the initial release, hence we loop through
					// The array to look for the respective release
					let theatrical = runtimes.find(v => v.version === '5fa05e7e-e46d-4993-a0c1-61ab5f29ccbf');
					let streaming = runtimes.find(v => v.version === '41d2bd1d-a7b9-4a65-ae0d-94936570ea65');
					let broadcasting = runtimes.find(v => v.version === '7d3f2265-0c9c-438e-b8f2-d2f506e93cac');

					if(theatrical) {
						// Theatrical release
						runtimeContainer.innerHTML = '<i class="icon material-icons md-18 md-av_timer"></i>' + theatrical.runtime + ' minutes';
					} else if(streaming) {
						// Streaming release
						runtimeContainer.innerHTML = '<i class="icon material-icons md-18 md-av_timer"></i>' + streaming.runtime + ' minutes';
					} else if(broadcasting) {
						// Broadcasting release
						runtimeContainer.innerHTML = '<i class="icon material-icons md-18 md-av_timer"></i>' + broadcasting.runtime + ' minutes';
					} else {
						// Otherwise we just pick the first entry
						runtimeContainer.innerHTML = '<i class="icon material-icons md-18 md-av_timer"></i>' + runtimes[0].runtime + ' minutes';
					}
				} else {
					// If there is only one runtime, we simply dislay that one
					runtimeContainer.innerHTML = '<i class="icon material-icons md-18 md-av_timer"></i>' + runtimes[0].runtime + ' minutes';
				}

				if(short) {
					runtimeContainer.innerHTML += ' (short film)';
				}

				runtimeContainer.classList.remove('empty');
			}

			if(budget){
				let included = movie.included;

				included.forEach(inc => {
					if(inc.type === 'paragraph--budget'){
						let amount = inc.attributes.field_amount;
						let currencyId = inc.relationships.field_currency.data.id;

						fetchTag('currency', currencyId)
							.then(tag => {
								let currencyCode = tag.data.attributes.name;
								budgetContainer.innerHTML += '<i class="icon material-icons-outline md-18 md-savings"></i>' + Number(amount).toLocaleString('en-GB')  + '&nbsp;' + currencyCode;
								budgetContainer.classList.remove('empty');

								// remove loader
								removeLoader();
							});
					}
				});
			}

			// Display technical aspects, if available
			if(aspect.length > 0 || colour.length > 0 || stock || audio.length > 0) {
				if(aspect.length > 0) {
					aspectContainer.innerHTML += '<i class="icon material-icons md-18 md-aspect_ratio"></i><ul></ul>';

					let list = aspectContainer.getElementsByTagName('ul')[0];
					aspects.forEach(asp => {
						list.innerHTML += '<li>' + typogr.typogrify(asp) + '</li>';
					});
				}

				if(colour.length > 0) {
					colourContainer.innerHTML += '<i class="icon material-icons-outline md-18 md-palette"></i><ul></ul>';

					let list = colourContainer.getElementsByTagName('ul')[0];
					colours.forEach(col => {
						list.innerHTML += '<li>' + typogr.typogrify(col) + '</li>';
					});
				}

				if(audio.length > 0) {
					audioContainer.innerHTML += '<i class="icon material-icons-outline md-18 md-speaker"></i><ul></ul>';

					let list = audioContainer.getElementsByTagName('ul')[0];
					audioArray.forEach(aud => {
						list.innerHTML += '<li>' + typogr.typogrify(aud) + '</li>';
					});
				}

				if(stock) {
					stockContainer.innerHTML += '<i class="icon material-icons-outline md-18 md-theaters"></i>' + stock;
				}

				// Remove empty class so the element has proper margin
				let technicalContainer = document.getElementById('technical');
				technicalContainer.classList.remove('empty');
			}

			// Now follows the other data
			if(indie) {
				indieContainer.innerHTML += '<i class="icon material-icons-outline md-18 md-rocket_launch"></i>Independent';
				indieContainer.classList.remove('empty');
			}

			if(poster != null) {
				poster = movie.data.attributes.field_poster.uri.split('File:').pop();
				posterContainer.innerHTML = '<img src="https://en.wikipedia.org/wiki/Special:Redirect/file?wptype=file&wpvalue=' + poster + '" title="Movie poster" />';
			}

			if(plot) {
				plot = plot.value;
			} else {
				plot = 'We don\'t have a plot for this movie';
			}

			if(original) {
				originalContainer.innerHTML = '<i class="icon material-icons md-18 md-language"></i>' + typogr.typogrify(original);
				originalContainer.classList.remove('empty');
			}

			if(release) {
				release = new Date(release);

				let day;
				let month;

				if(release.getDate() < 10) {
					day = '0' + release.getDate();
				} else {
					day = release.getDate();
				}

				if((release.getMonth() + 1) < 10) {
					month = '0' + (release.getMonth() + 1);
				} else {
					month = release.getMonth() + 1;
				}
				releaseContainer.innerHTML = '<i class="icon material-icons md-18 md-today"></i>' + day + '.' + month + '.' + release.getFullYear();
			}

			if(country.length > 0) {
				countryContainer.innerHTML = '<i class="icon material-icons md-18 md-public"></i><ul></ul>';
				let list = countryContainer.getElementsByTagName('ul')[0];

				for (let i = 0; i < country.length; i++) {
					getCountryName(country[i])
						.then(res => {
							list.innerHTML += '<li>' + typogr.typogrify(res) + '</li>';
						})
						.catch(() => {
							displayMessage({
								type: 'error',
								message: 'There was a problem displaying the country name.'
							});
						});
				}
			}

			if(language.length > 0) {
				languageContainer.innerHTML = '<i class="icon material-icons md-18 md-translate"></i><ul></ul>';
				let list = languageContainer.getElementsByTagName('ul')[0];

				language.forEach(lang => {
					lang = lang.substring(0,3); // ISO 639-3 only needs 3 characters
					getLanguageName(lang)
						.then(res => {
							res = res.split(';', 1)[0];
							list.innerHTML += '<li>' + typogr.typogrify(res) + '</li>';
						})
						.catch(() => {
							displayMessage({
								type: 'error',
								message: 'There was a problem displaying the languages.'
							});
						});
				});
			}

			if(website) {
					websiteContainer.innerHTML = '<i class="icon material-icons md-18 md-link"></i><a href="' + website.uri + '" alt="Website" title="Website" target="_blank">Website</a>';
				}

			if(wikipedia||wikidata||imdb||omdb||tmdb||thetvdb||metacritic||rottentomatoes) {
				linksContainer.innerHTML = '<i class="icon material-icons-outline md-18 md-info"></i><ul></ul>';
				let list = linksContainer.getElementsByTagName('ul')[0];

				if(wikipedia) {
					list.innerHTML += '<li><a href="' + wikipedia.uri + '" alt="Wikipedia" title="Wikipedia" target="_blank">Wikipedia</a></li>';
				}

				if(wikidata) {
					list.innerHTML += '<li><a href="https://www.wikidata.org/wiki/' + wikidata + '" alt="Wikidata" title="Wikidata" target="_blank">Wikidata</a></li>';
				}

				if(imdb) {
					list.innerHTML += '<li><a href="https://www.imdb.com/title/' + imdb + '" alt="Internet Movie Database" title="Internet Movie Database" target="_blank">IMDb</a></li>';
				}

				if(omdb) {
					list.innerHTML += '<li><a href="https://www.omdb.org/movie/' + omdb + '" alt="Open Media Database" title="Open Media Database" target="_blank">omdb</a></li>';
				}

				if(tmdb) {
					list.innerHTML += '<li><a href="https://www.themoviedb.org/movie/' + tmdb + '" alt="The Movie Database" title="The Movie Database" target="_blank">TMDb</a></li>';
				}

				if(thetvdb) {
					list.innerHTML += '<li><a href="https://www.thetvdb.com/dereferrer/movie/' + thetvdb + '" alt="TheTVDb.com" title="TheTVDb.com" target="_blank">TheTVDb.com</a></li>';
				}

				if(metacritic) {
					list.innerHTML += '<li><a href="' + metacritic.uri + '" alt="Metacritic" title="Metacritic" target="_blank">Metacritic</a></li>';
				}

				if(rottentomatoes) {
					list.innerHTML += '<li><a href="' + rottentomatoes.uri + '" alt="Rotten Tomatoes" title="Rotten Tomatoes" target="_blank">Rotten Tomatoes</a></li>';
				}
			}

			PAGE_TITLE.innerHTML = typogr.typogrify(title);
			plotContainer.innerHTML = '<p>' + typogr.typogrify(plot) + '</p>';

			// Set the correct meta tags
			let keywords = [];
			if(original) {
				keywords = [title.replace(/"/gm, '\''), original.replace(/"/gm, '\'')];
			} else {
				keywords = [title.replace(/"/gm, '\'')];
			}

			// Remove loader if we don't have a budget (otherwise it was removed there)
			if(!budget) {
				removeLoader();
			}

			metaTags({
				'title': title.replace(/"/gm, '\''),
				'description': plot.replace(/"/gm, '\''),
				'keywords': keywords
			});

			// Set the correct open graphs
			openGraph({
				'title': title.replace(/"/gm, '\''),
				'description': plot.replace(/"/gm, '\''),
				'type': 'video.movie'
			});
		})
		.catch(() => {
			displayMessage({
				type: 'error',
				message: 'We had problems displaying the content. Please try again later.'
			});
		});
}

function displayTVShow() {
	let id = PATH.split('/tv-show/').pop(); // Get movie ID from URL
	let inc = ['field_genre', 'field_network', 'field_production_company', 'field_episodes', 'field_aspect_ratio', 'field_colour', 'field_audio_format']; // The fields we want to include in our JSON request

	// Get container elements
	let posterContainer = document.getElementById('poster');
	let originalContainer = document.getElementById('original-title');
	let genreContainer = document.getElementById('genre');
	let statusContainer = document.getElementById('status');
	document.getElementById('release-date');
	let plotContainer = document.getElementById('plot');
	let countryContainer = document.getElementById('country');
	let languageContainer = document.getElementById('language');
	let websiteContainer = document.getElementById('website');
	let networkContainer = document.getElementById('network');
	let productionContainer = document.getElementById('production-company');
	let aspectContainer = document.getElementById('aspect-ratio');
	let audioContainer = document.getElementById('audio-format');
	let colourContainer = document.getElementById('colour');
	let linksContainer = document.getElementById('links');

	fetchFilm('tv_show', id, inc)
		.then(show => {
			// If there is a 404 error, we display an error page and stop the execution of the function
			if(show.errors && show.errors[0].status == 404) {
				pageNotFound('tv_show');
				return;
			}

			// Define data points
			let title = show.data.attributes.title;
			let poster = show.data.attributes.field_poster;
			let original = show.data.attributes.field_original_title;
			show.data.attributes.field_release_date;
			let status = show.data.attributes.field_status;
			let plot = show.data.attributes.body;
			let network = show.data.relationships.field_network.data;
			let production = show.data.relationships.field_production_company.data;
			let language = show.data.attributes.field_language;
			let country = show.data.attributes.field_country;
			let website = show.data.attributes.field_website;
			let wikidata = show.data.attributes.field_wikidata;
			let wikipedia = show.data.attributes.field_wikipedia;
			let imdb = show.data.attributes.field_imdb;
			let omdb = show.data.attributes.field_omdb;
			let thetvdb = show.data.attributes.field_thetvdb;
			let tmdb = show.data.attributes.field_tmdb;
			let metacritic = show.data.attributes.field_metacritic;
			let rottentomatoes = show.data.attributes.field_rotten_tomatoes;
			let aspect = show.data.relationships.field_aspect_ratio.data;
			let audio = show.data.relationships.field_audio_format.data;
			let colour = show.data.relationships.field_colour.data;

			// First we loop through the included field of the json data and populate
			// The needed data accordingly. We'll use that data to display special content
			let genres = [];
			let netArray = [];
			let prodArray = [];
			let episodes = [];
			let aspects = [];
			let colours = [];
			let audioArray = [];

			if(show.included) {
				show.included.forEach(term => {
					// Get TV show genre(s)
					if(term.type == 'taxonomy_term--genre') {
						genres.push(term.attributes.name);
					}

					// Get network(s)
					if (network.length > 0) {
						network.forEach(dist => {
							if(term.id == dist.id) {
								netArray.push(term.attributes.name);
							}
						});
					}

					// Get production company(-ies)
					if (production.length > 0) {
						production.forEach(prod => {
							if(term.id == prod.id) {
								prodArray.push(term.attributes.name);
							}
						});
					}

					// Get aspect ratio(s)
					if (aspect.length > 0) {
						aspect.forEach(asp => {
							if(term.id == asp.id) {
								aspects.push(term.attributes.name);
							}
						});
					}

					// Get colours
					if (colour.length > 0) {
						colour.forEach(col => {
							if(term.id == col.id) {
								colours.push(term.attributes.name);
							}
						});
					}

					// Get audio formats
					if (audio.length > 0) {
						audio.forEach(aud => {
							if(term.id == aud.id) {
								audioArray.push(term.attributes.name);
							}
						});
					}

					// Get episodes
					if(term.type == 'paragraph--episode' && term.relationships.field_episode_title.data != null) {
						let episode = {
							season: term.attributes.field_season,
							episode: term.attributes.field_episode_number,
							id: term.relationships.field_episode_title.data.id
						};

						episodes.push(episode);
					}
				});
			}

			// Now we populate the data we gained from that loop before
			if(genres.length > 0) {
				genreContainer.classList.remove('empty');
				genreContainer.innerHTML = '<i class="icon material-icons md-18 md-local_offer"></i><ul></ul>';
				let list = genreContainer.getElementsByTagName('ul')[0];
				genres.forEach(genre => {
				list.innerHTML += '<li>' + typogr.typogrify(genre) + '</li>';
				});
			}

			if (network.length > 0) {
				network.forEach(net => {
					show.included.forEach(term => {
						if(term.id == net.id) {
							netArray.push(term.attributes.name);
						}
					});
				});

				netArray = [...new Set(netArray)]; // In case a company is network AND production company, there will be duplicates we remove here

				networkContainer.innerHTML = '<i class="icon material-icons md-18 md-tv"></i><ul></ul>';
				let list = networkContainer.getElementsByTagName('ul')[0];
				netArray.forEach(dist => {
					list.innerHTML += '<li>' + typogr.typogrify(dist) + '</li>';
				});
			}

			if (production.length > 0) {
				production.forEach(prod => {
					show.included.forEach(term => {
						if(term.id == prod.id) {
							prodArray.push(term.attributes.name);
						}
					});
				});

				prodArray = [...new Set(prodArray)]; // In case a company is network AND production company, there will be duplicates we remove here

				productionContainer.innerHTML += '<i class="icon material-icons md-18 md-handyman"></i><ul></ul>';
				let list = productionContainer.getElementsByTagName('ul')[0];
				prodArray.forEach(prod => {
					list.innerHTML += '<li>' + typogr.typogrify(prod) + '</li>';
				});
			}

			// Display technical aspects, if available
			if(aspect.length > 0 || colour.length > 0 || audio.length > 0) {
				if(aspect.length > 0) {
					aspectContainer.innerHTML += '<i class="icon material-icons md-18 md-aspect_ratio"></i><ul></ul>';

					let list = aspectContainer.getElementsByTagName('ul')[0];
					aspects.forEach(asp => {
						list.innerHTML += '<li>' + typogr.typogrify(asp) + '</li>';
					});
				}

				if(colour.length > 0) {
					colourContainer.innerHTML += '<i class="icon material-icons-outline md-18 md-palette"></i><ul></ul>';

					let list = colourContainer.getElementsByTagName('ul')[0];
					colours.forEach(col => {
						list.innerHTML += '<li>' + typogr.typogrify(col) + '</li>';
					});
				}

				if(audio.length > 0) {
					audioContainer.innerHTML += '<i class="icon material-icons-outline md-18 md-speaker"></i><ul></ul>';

					let list = audioContainer.getElementsByTagName('ul')[0];
					audioArray.forEach(aud => {
						list.innerHTML += '<li>' + typogr.typogrify(aud) + '</li>';
					});
				}

				// Remove empty class so the element has proper margin
				let technicalContainer = document.getElementById('technical');
				technicalContainer.classList.remove('empty');
			}

			// Now follows the other data
			if(poster != null) {
				poster = show.data.attributes.field_poster.uri.split('File:').pop();
				posterContainer.innerHTML = '<img src="https://en.wikipedia.org/wiki/Special:Redirect/file?wptype=file&wpvalue=' + poster + '" title="TV show poster" />';
			}

			if(plot) {
				plot = plot.value;
			} else {
				plot = 'We don\'t have a plot for this TV show';
			}

			if(original) {
				originalContainer.innerHTML = '<i class="icon material-icons md-18 md-language"></i>' + original;
				originalContainer.classList.remove('empty');
			}

			if(status) {
				statusContainer.innerHTML = '<i class="icon material-icons md-18 md-cast"></i>' + status;
				statusContainer.classList.remove('empty');
			}

			if(country.length > 0) {
				countryContainer.innerHTML = '<i class="icon material-icons md-18 md-public"></i><ul></ul>';
				let list = countryContainer.getElementsByTagName('ul')[0];

				for (let i = 0; i < country.length; i++) {
					getCountryName(country[i])
						.then(res => {
							list.innerHTML += '<li>' + typogr.typogrify(res) + '</li>';
						})
						.catch(() => {
							displayMessage({
								type: 'error',
								message: 'There was a problem displaying the country name.'
							});
						});
				}
			}

			if(language.length > 0) {
				languageContainer.innerHTML = '<i class="icon material-icons md-18 md-translate"></i><ul></ul>';
				let list = languageContainer.getElementsByTagName('ul')[0];

				language.forEach(lang => {
					lang = lang.substring(0,3); // ISO 639-3 only needs 3 characters
					getLanguageName(lang)
						.then(res => {
							res = res.split(';', 1)[0];
							list.innerHTML += '<li>' + typogr.typogrify(res) + '</li>';
						})
						.catch(() => {
							displayMessage({
								type: 'error',
								message: 'There was a problem displaying the languages.'
							});
						});
				});
			}

			if(website) {
					websiteContainer.innerHTML = '<i class="icon material-icons md-18 md-link"></i><a href="' + website.uri + '" alt="Website" title="Website" target="_blank">Website</a>';
				}

			if(wikipedia||wikidata||imdb||omdb||tmdb||thetvdb||metacritic||rottentomatoes) {
				linksContainer.innerHTML = '<i class="icon material-icons-outline md-18 md-info"></i><ul></ul>';
				let list = linksContainer.getElementsByTagName('ul')[0];

				if(wikipedia) {
					list.innerHTML += '<li><a href="' + wikipedia.uri + '" alt="Wikipedia" title="Wikipedia" target="_blank">Wikipedia</a></li>';
				}

				if(wikidata) {
					list.innerHTML += '<li><a href="https://www.wikidata.org/wiki/' + wikidata + '" alt="Wikidata" title="Wikidata" target="_blank">Wikidata</a></li>';
				}

				if(imdb) {
					list.innerHTML += '<li><a href="https://www.imdb.com/title/' + imdb + '" alt="Internet Movie Database" title="Internet Movie Database" target="_blank">IMDb</a></li>';
				}

				if(omdb) {
					list.innerHTML += '<li><a href="https://www.omdb.org/movie/' + omdb + '" alt="Open Media Database" title="Open Media Database" target="_blank">omdb</a></li>';
				}

				if(tmdb) {
					list.innerHTML += '<li><a href="https://www.themoviedb.org/tv/' + tmdb + '" alt="The Movie Database" title="The Movie Database" target="_blank">TMDb</a></li>';
				}

				if(thetvdb) {
					list.innerHTML += '<li><a href="https://www.thetvdb.com/dereferrer/series/' + thetvdb + '" alt="TheTVDb.com" title="TheTVDb.com" target="_blank">TheTVDb.com</a></li>';
				}

				if(metacritic) {
					list.innerHTML += '<li><a href="' + metacritic.uri + '" alt="Metacritic" title="Metacritic" target="_blank">Metacritic</a></li>';
				}

				if(rottentomatoes) {
					list.innerHTML += '<li><a href="' + rottentomatoes.uri + '" alt="Rotten Tomatoes" title="Rotten Tomatoes" target="_blank">Rotten Tomatoes</a></li>';
				}
			}

			// Get episodes
			if(episodes.length > 0) {
				// First we start with an empty array, set a heading and a table
				let episodeList = [];
				let episodesContainer = document.getElementById('episodes');
				episodesContainer.innerHTML += '<h2>Episodes</h2>';
				episodesContainer.innerHTML += '<table><thead><tr><th><abbr title="Season">S</abbr>/<abbr title="Episode">E</abbr></th><th>Description</th><th>Aired</th><th>Runtime</th></tr></thead><tbody></tbody></table>';
				displayLoader('episodes');
				let table = episodesContainer.getElementsByTagName('table')[0];
				let thead = table.getElementsByTagName('thead')[0];
				let tbody = table.getElementsByTagName('tbody')[0];
				thead.children[0].style.top = headerHeight() + 'px';

				// Now we populate the table
				episodes.forEach(ep => {
					let number = episodes.indexOf(ep) + 1; // We'll need that later for the proper order (due to specials with season nr 0)

					fetchFilm('episode', ep.id)
						.then(res => {
							let data = res.data;
							let title = data.attributes.title;
							let original = data.attributes.field_original_title;
							let plot = data.attributes.body;
							let release = data.attributes.field_release_date;
							let runtime = data.attributes.field_episode_runtime;

							if(plot) {
								plot = plot.value;
							}

							let extEp = {
								'number': number,
								'season': ep.season,
								'episode': ep.episode,
								'title': title,
								'original': original,
								'plot': plot,
								'release': release,
								'runtime': runtime
							};

							episodeList.push(extEp);
							if(episodeList.length == episodes.length) {
								episodeList.sort((a, b) => a.number - b.number);

								episodeList.forEach(ep => {
									let season = ep.season;
									let episode = ep.episode;
									let title = ep.title;
									let original = ep.original;
									let plot = ep.plot;
									let release = ep.release;
									let runtime = ep.runtime;

									if(season < 10) {
										season = '0' + season;
									}

									if(episode < 10) {
										episode = '0' + episode;
									}

									if(original) {
										title = typogr.typogrify('<span class="title">' + title + '</span> (' + original + ')');
									} else {
										title = typogr.typogrify('<span class="title">' + title + '</span>');
									}

									if (plot) {
										plot = typogr.typogrify(plot);
									} else {
										plot = '';
									}

									if(release) {
										release = new Date(release);
										let day;
										let month;

										if(release.getDate() < 10) {
											day = '0' + release.getDate();
										} else {
											day = release.getDate();
										}

										if((release.getMonth() + 1) < 10) {
											month = '0' + (release.getMonth() + 1);
										} else {
											month = release.getMonth() + 1;
										}

										release = day + '.' + month + '.' + release.getFullYear();
									} else {
										release = '';
									}

									if(runtime) {
										runtime = runtime + '&nbsp;minutes';
									} else {
										runtime = '';
									}

									tbody.innerHTML += '<tr><td>S' + season + '&nbsp;E' + episode + '</td><td>' + title + '<br />' + plot + '</td><td>' + release + '</td><td>' + runtime + '</td></tr>';
								});

								removeLoader();
							}
						})
						.catch(() => {
							displayMessage({
								type: 'error',
								message: 'There was a problem displaying the episodes. Please try again later.'
							});
						});
				});
			}

			PAGE_TITLE.innerHTML = typogr.typogrify(title);
			plotContainer.innerHTML = '<p>' + typogr.typogrify(plot) + '</p>';

			// Set the correct meta tags
			let keywords = [];
			if(original) {
				keywords = [title.replace(/"/gm, '\''), original.replace(/"/gm, '\'')];
			} else {
				keywords = [title.replace(/"/gm, '\'')];
			}

			metaTags({
				'title': title.replace(/"/gm, '\''),
				'description': plot.replace(/"/gm, '\''),
				'keywords': keywords
			});

			// Set the correct open graphs
			openGraph({
				'title': title.replace(/"/gm, '\''),
				'description': plot.replace(/"/gm, '\''),
				'type': 'video.tv_show'
			});
		})
		.catch(() => {
			displayMessage({
				type: 'error',
				message: 'We had a problem displaying the content. Please try again later.'
			});
		});
}

function buildSearchData(search) {
	let filter = '&filter[title-group][group][conjunction]=OR&filter[title-filter][condition][path]=title&filter[title-filter][condition][operator]=CONTAINS&filter[title-filter][condition][value]=' + search + '&filter[title-filter][condition][memberOf]=title-group&filter[original-filter][condition][path]=field_original_title&filter[original-filter][condition][operator]=CONTAINS&filter[original-filter][condition][value]=' + search + '&filter[original-filter][condition][memberOf]=title-group'; // We create a filter group to search the title and original title
	let movies = fetchFilms('movie', config.searchResultsPerType, 0, filter, 'title');
	let shows = fetchFilms('tv_show', config.searchResultsPerType, 0, filter, 'title');

	Promise.all([movies, shows])
		.then(res => {
			let films = [];
			let total = res[0].data.length + res[1].data.length;

			if(total == 0) {
				SUGGESTIONS.innerHTML = typogr.typogrify('<div class="messages">No results found...</div>');
				return;
			}

			res.forEach(type => {
				type.data.forEach(film => {
					let id = film.id;
					let title = film.attributes.title;
					let type = film.type.split('--').pop();

					film = {
						'id': id,
						'title': title,
						'type': type
					};

					films.push(film);

					if(films.length == total) {
						displaySearchData(films);
					}
				});
			});
		})
		.catch(err => console.log(err));
}

function displaySearchData(films) {
	SUGGESTIONS.innerHTML = '<ul></ul>';
	let list = SUGGESTIONS.getElementsByTagName('ul')[0];

	films.sort((a, b) => (a.title > b.title) ? 1 : ((b.title > a.title) ? -1 : 0));
	films.forEach(film => {
		let id = film.id;
		let title = film.title;
		let type = film.type;
		let urlprefix;

		if(type == 'movie') {
			type = '<i class="icon material-icons md-18 md-movie"></i>';
			urlprefix = 'movie/';
		} else {
			type = '<i class="icon material-icons md-18 md-tv"></i>';
			urlprefix = 'tv-show/';
		}

		list.innerHTML += '<li><a href="/' + urlprefix + id + '" alt="' + title + '" title="' + title + '">' + type + '<span>' + typogr.typogrify(title) + '</span></a></li>';
	});
}

function search() {
	if(SEARCHBOX.value.length == 0) {
		SUGGESTIONS.classList.remove('show');
	} else if(SEARCHBOX.value.length > 0 && SEARCHBOX.value.length < 3) {
		SUGGESTIONS.innerHTML = typogr.typogrify('<div class="messages">Please enter three or more characters to search.</div>');
		SUGGESTIONS.classList.add('show');
	} else {
		SUGGESTIONS.innerHTML = '';
		displayLoader('suggestions');
		buildSearchData(SEARCHBOX.value);
		SUGGESTIONS.classList.add('show');
	}
}

function searchListener() {
	// Trigger search
	SEARCHBOX.addEventListener('keyup', search);

	// Toggle suggestions visibility on click in- our outside suggestions
	document.body.addEventListener('click', function (event) {
		let searchContainer = document.getElementById('search');

		if(!searchContainer.contains(event.target) && SUGGESTIONS.classList.contains('show')) {
			SUGGESTIONS.classList.remove('show');
		} else {
			if(SEARCHBOX.value != '') {
				SUGGESTIONS.classList.add('show');
			}
		}
	});
}

// init app
(function init() {
	buildMenus();
	contentOffset();
	displayContent();
	searchListener();
})();
